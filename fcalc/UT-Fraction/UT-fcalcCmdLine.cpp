#include "pch.h"
#include "CppUnitTest.h"
#include "../fcalc/fcalcCmdLine.h"
#include <memory>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UTfcalcCmdLine
{

	// 01 - fcalc
	// 02 - fcalc -v
	// 03 - fcalc -d
	// 04 - fcalc -f formula.txt
	// 05 - fcalc 1+2+3
	// 06 - fcalc -v 1+2+3
	// 07 - fcalc -d 1+2+3
	// 08 - fcalc -v -f formula.txt
	// 09 - fcalc -f formula.txt -d
	// 10 - fcalc -d -v
	// 11 - fcalc -v -d 1+2+3
	// 12 - fcalc -d -f formula.txt - v
	// 13 - fcalc -v 1+2+3 -f formula.txt
	// 14 - fcalc -h

	TEST_CLASS(UTfcalcCmdLine)
	{
	private:
		class ArgVector
		{
		public:
			const size_t numArgs = 10;
			const size_t argSize = 100;
			char** argv;
			ArgVector()
			{
				argv = new char* [numArgs];
				for (size_t n = 0; n < numArgs; n++)
				{
					argv[n] = new char[argSize];
					argv[n][0] = '\0';
				}
			}
			~ArgVector()
			{
				for (size_t n = 0; n < numArgs; n++)
				{
					delete[] argv[n];
				}
				delete[] argv;
			}
		};

	public:
		TEST_METHOD(fcalcCmdLine_1)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			try
			{
				fcalcCmdLine cmdLine(1, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), true);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(fcalcCmdLine_2)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-v");
			try
			{
				fcalcCmdLine cmdLine(2, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), true);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(fcalcCmdLine_3)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-d");
			try
			{
				fcalcCmdLine cmdLine(2, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), false);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), true);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(fcalcCmdLine_4)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[2], argv.argSize - 1, "..\\..\\UT-Fraction\\formula.txt");
			try
			{
				fcalcCmdLine cmdLine(3, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "4 * 5 * 6", true);
				string line3 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line3 == "9/10", true);
				string line4 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line4 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(fcalcCmdLine_5)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "1+2+3");
			try
			{
				fcalcCmdLine cmdLine(2, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
		
		TEST_METHOD(fcalcCmdLine_6)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-v");
			strcpy_s(argv.argv[2], argv.argSize - 1, "1+2+3");
			try
			{
				fcalcCmdLine cmdLine(3, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(fcalcCmdLine_7)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-d");
			strcpy_s(argv.argv[2], argv.argSize - 1, "1+2+3");
			try
			{
				fcalcCmdLine cmdLine(3, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), false);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		// 08 - fcalc -v -f formula.txt
		TEST_METHOD(fcalcCmdLine_8)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-v");
			strcpy_s(argv.argv[2], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[3], argv.argSize - 1, "..\\..\\UT-Fraction\\formula.txt");
			try
			{
				fcalcCmdLine cmdLine(4, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "4 * 5 * 6", true);
				string line3 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line3 == "9/10", true);
				string line4 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line4 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
		
		TEST_METHOD(fcalcCmdLine_9)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[2], argv.argSize - 1, "..\\..\\UT-Fraction\\formula.txt");
			strcpy_s(argv.argv[3], argv.argSize - 1, "-d");
			try
			{
				fcalcCmdLine cmdLine(4, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), false);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "4 * 5 * 6", true);
				string line3 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line3 == "9/10", true);
				string line4 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line4 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}

		TEST_METHOD(fcalcCmdLine_10)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-d");
			strcpy_s(argv.argv[2], argv.argSize - 1, "-v");
			try
			{
				fcalcCmdLine cmdLine(3, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), false);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), true);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
		
		TEST_METHOD(fcalcCmdLine_11)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-v");
			strcpy_s(argv.argv[2], argv.argSize - 1, "-d");
			strcpy_s(argv.argv[3], argv.argSize - 1, "1+2+3");
			try
			{
				fcalcCmdLine cmdLine(4, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), false);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
		
		TEST_METHOD(fcalcCmdLine_12)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-d");
			strcpy_s(argv.argv[2], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[3], argv.argSize - 1, "..\\..\\UT-Fraction\\formula.txt");
			strcpy_s(argv.argv[4], argv.argSize - 1, "-v");
			try
			{
				fcalcCmdLine cmdLine(5, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), false);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "4 * 5 * 6", true);
				string line3 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line3 == "9/10", true);
				string line4 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line4 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
		
		TEST_METHOD(fcalcCmdLine_13)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-v");
			strcpy_s(argv.argv[2], argv.argSize - 1, "1+2+3");
			strcpy_s(argv.argv[3], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[4], argv.argSize - 1, "..\\..\\UT-Fraction\\formula.txt");
			try
			{
				fcalcCmdLine cmdLine(5, argv.argv);
			}
			catch (string s)
			{
				Assert::IsTrue(s.size() > 1);
			}
		}

		TEST_METHOD(fcalcCmdLine_14)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-h");
			try
			{
				fcalcCmdLine cmdLine(2, argv.argv);
			}
			catch (string s)
			{
				Assert::IsTrue(s.size() > 1);
			}
		}

		TEST_METHOD(fcalcCmdLine_non_exist_formula_file)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[2], argv.argSize - 1, "non_exist_formula_file");
			try
			{
				fcalcCmdLine cmdLine(3, argv.argv);
			}
			catch (string s)
			{
				Assert::IsTrue(s.size() > 1);
			}
		}

		TEST_METHOD(fcalcCmdLine_empty_formula_file)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-f");
			strcpy_s(argv.argv[2], argv.argSize - 1, "..\\..\\UT-Fraction\\empty.txt");
			try
			{
				fcalcCmdLine cmdLine(3, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), false);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
		TEST_METHOD(Text2Items_unquoted_formula_at_commandline)
		{
			ArgVector argv;
			strcpy_s(argv.argv[0], argv.argSize - 1, "fcalc");
			strcpy_s(argv.argv[1], argv.argSize - 1, "-v");
			strcpy_s(argv.argv[2], argv.argSize - 1, "1+2+3");
			strcpy_s(argv.argv[3], argv.argSize - 1, "+");
			strcpy_s(argv.argv[4], argv.argSize - 1, "4");
			try
			{
				fcalcCmdLine cmdLine(5, argv.argv);
				Assert::AreEqual<bool>(cmdLine.OutputInFraction(), true);
				Assert::AreEqual<bool>(cmdLine.InputFromStdin(), false);
				Assert::AreEqual<bool>(cmdLine.Verbose(), true);
				string line1 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line1 == "1+2+3 + 4", true);
				string line2 = cmdLine.GetNextTextFormula();
				Assert::AreEqual<bool>(line2 == "", true);
			}
			catch (string s)
			{
				Assert::Fail();
			}
		}
	};
}