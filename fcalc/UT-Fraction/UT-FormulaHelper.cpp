#include "pch.h"
#include "CppUnitTest.h"
#include "../fcalc/FormulaHelper.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UTFormulaHelper
{
	TEST_CLASS(UTFormulaHelper)
	{
	public:
		TEST_METHOD(Text2Items_OnePlusTwentyThree)
		{
			Formula::FormulaHelper helper;
			string formula = "1 + 23";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<size_t>(items.size(), 3);
			Assert::AreEqual<bool>((items[0].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[0].value, "1");
			Assert::AreEqual<bool>((items[1].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[1].value, "+");
			Assert::AreEqual<bool>((items[2].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[2].value, "23");

			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);
		}

		TEST_METHOD(Text2Items_OnePlusTwentyThreeWithSpaces)
		{
			Formula::FormulaHelper helper;
			string formula = "  1 +  23 ";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<size_t>(items.size(), 3);
			Assert::AreEqual<bool>((items[0].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[0].value, "1");
			Assert::AreEqual<bool>((items[1].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[1].value, "+");
			Assert::AreEqual<bool>((items[2].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[2].value, "23");
		}

		TEST_METHOD(Text2Items_IntegerWithMultipleBrackets)
		{
			Formula::FormulaHelper helper;
			string formula = "  [(1 +  23) * 8] - 6/24 + {-1+4/5/2}";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<size_t>(items.size(), 24);
			Assert::AreEqual<bool>((items[0].type == Formula::ItemType::open_bracket), true);
			Assert::AreEqual<string>(items[0].value, "[");
			Assert::AreEqual<bool>((items[1].type == Formula::ItemType::open_bracket), true);
			Assert::AreEqual<string>(items[1].value, "(");
			Assert::AreEqual<bool>((items[2].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[2].value, "1");
			Assert::AreEqual<bool>((items[3].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[3].value, "+");
			Assert::AreEqual<bool>((items[4].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[4].value, "23");
			Assert::AreEqual<bool>((items[5].type == Formula::ItemType::close_bracket), true);
			Assert::AreEqual<string>(items[5].value, ")");
			Assert::AreEqual<bool>((items[6].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[6].value, "*");
			Assert::AreEqual<bool>((items[7].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[7].value, "8");
			Assert::AreEqual<bool>((items[8].type == Formula::ItemType::close_bracket), true);
			Assert::AreEqual<string>(items[8].value, "]");
			Assert::AreEqual<bool>((items[9].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[9].value, "-");
			Assert::AreEqual<bool>((items[10].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[10].value, "6");
			Assert::AreEqual<bool>((items[11].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[11].value, "/");
			Assert::AreEqual<bool>((items[12].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[12].value, "24");
			Assert::AreEqual<bool>((items[13].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[13].value, "+");
			Assert::AreEqual<bool>((items[14].type == Formula::ItemType::open_bracket), true);
			Assert::AreEqual<string>(items[14].value, "{");
			Assert::AreEqual<bool>((items[15].type == Formula::ItemType::sign), true);
			Assert::AreEqual<string>(items[15].value, "-");
			Assert::AreEqual<bool>((items[16].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[16].value, "1");
			Assert::AreEqual<bool>((items[17].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[17].value, "+");
			Assert::AreEqual<bool>((items[18].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[18].value, "4");
			Assert::AreEqual<bool>((items[19].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[19].value, "/");
			Assert::AreEqual<bool>((items[20].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[20].value, "5");
			Assert::AreEqual<bool>((items[21].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[21].value, "/");
			Assert::AreEqual<bool>((items[22].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[22].value, "2");
			Assert::AreEqual<bool>((items[23].type == Formula::ItemType::close_bracket), true);
			Assert::AreEqual<string>(items[23].value, "}");

			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);
		}

		TEST_METHOD(Text2Items_FractionWithWholeNumberAndDecimal)
		{
			Formula::FormulaHelper helper;
			string formula = "+1 5 / 23 + ( -2   1/3)*1.5 ";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<size_t>(items.size(), 15);
			Assert::AreEqual<bool>((items[0].type == Formula::ItemType::sign), true);
			Assert::AreEqual<string>(items[0].value, "+");
			Assert::AreEqual<bool>((items[1].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[1].value, "1");
			Assert::AreEqual<bool>((items[2].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[2].value, "5");
			Assert::AreEqual<bool>((items[3].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[3].value, "/");
			Assert::AreEqual<bool>((items[4].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[4].value, "23");
			Assert::AreEqual<bool>((items[5].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[5].value, "+");
			Assert::AreEqual<bool>((items[6].type == Formula::ItemType::open_bracket), true);
			Assert::AreEqual<string>(items[6].value, "(");
			Assert::AreEqual<bool>((items[7].type == Formula::ItemType::sign), true);
			Assert::AreEqual<string>(items[7].value, "-");
			Assert::AreEqual<bool>((items[8].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[8].value, "2");
			Assert::AreEqual<bool>((items[9].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[9].value, "1");
			Assert::AreEqual<bool>((items[10].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[10].value, "/");
			Assert::AreEqual<bool>((items[11].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[11].value, "3");
			Assert::AreEqual<bool>((items[12].type == Formula::ItemType::close_bracket), true);
			Assert::AreEqual<string>(items[12].value, ")");
			Assert::AreEqual<bool>((items[13].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[13].value, "*");
			Assert::AreEqual<bool>((items[14].type == Formula::ItemType::decimal_number), true);
			Assert::AreEqual<string>(items[14].value, "1.5");

			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);
		}

		TEST_METHOD(Text2Items_FractionWithDecimalNumerator)
		{
			Formula::FormulaHelper helper;
			string formula = "6 1.5/23";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<size_t>(items.size(), 4);
			Assert::AreEqual<bool>((items[0].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[0].value, "6");
			Assert::AreEqual<bool>((items[1].type == Formula::ItemType::decimal_number), true);
			Assert::AreEqual<string>(items[1].value, "1.5");
			Assert::AreEqual<bool>((items[2].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[2].value, "/");
			Assert::AreEqual<bool>((items[3].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[3].value, "23");
		}
		TEST_METHOD(Text2Items_TwoConsecutiveOperators)
		{
			Formula::FormulaHelper helper;
			string formula = "1++2";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<size_t>(items.size(), 4);
			Assert::AreEqual<bool>((items[0].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[0].value, "1");
			Assert::AreEqual<bool>((items[1].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[1].value, "+");
			Assert::AreEqual<bool>((items[2].type == Formula::ItemType::math_operator), true);
			Assert::AreEqual<string>(items[2].value, "+");
			Assert::AreEqual<bool>((items[3].type == Formula::ItemType::digits), true);
			Assert::AreEqual<string>(items[3].value, "2");
		}

		TEST_METHOD(Text2Items_InvalidOperator)
		{
			Formula::FormulaHelper helper;
			string formula = "1&2";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(Text2Items_InvalidDecimal)
		{
			Formula::FormulaHelper helper;
			string formula = "1.2.3";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(Text2Items_SpaceAfterDecimalPoint)
		{
			Formula::FormulaHelper helper;
			string formula = "1. 23";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(Text2Items_NoDigitAfterDecimalPoint)
		{
			Formula::FormulaHelper helper;
			string formula = "1. + 2.3";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, false);
		}
		TEST_METHOD(Text2Items_Invalid_TwoConsecutiveSigns)
		{
			Formula::FormulaHelper helper;
			string formula = "--1.2";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(CheckBracketSanity_CloseBracketNotMatchingOpenBracket)
		{
			Formula::FormulaHelper helper;
			string formula = "1.3 * (1+4]";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(CheckBracketSanity_CloseBracketNotMatchingOpenBracketNested)
		{
			Formula::FormulaHelper helper;
			string formula = "1.3 * {(4-(1+4]) / [3/4+1] (1) }";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(CheckBracketSanity_TooManyCloseBrackets)
		{
			Formula::FormulaHelper helper;
			string formula = "1.3 * {(4-1)*3}}";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(CheckBracketSanity_TooManyOpenBrackets)
		{
			Formula::FormulaHelper helper;
			string formula = "1.3 * {(4-1)*[3}";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, false);
		}

		TEST_METHOD(TextToFormula_1)
		{
			Formula::FormulaHelper helper;
			string formula = "-1.3 * 2 4/5 / 1.2 * {2 + 4/5 - 18/3/4 * [6+(-8-1)]}";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);

			vector<Formula::FormulaElement> output;
			b = helper.ItemsToFormula(items, output);
			Assert::AreEqual<bool>(b, true);

			Assert::AreEqual<bool>(output[0].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[0].number.Sign() == -1, true);
			Assert::AreEqual<bool>(output[0].number.WholeNum() == 1, true);
			Assert::AreEqual<bool>(output[0].number.Numerator() == 3, true);
			Assert::AreEqual<bool>(output[0].number.Denominator() == 10, true);

			Assert::AreEqual<bool>(output[1].type == Formula::FormulaElementType::multiply, true);

			Assert::AreEqual<bool>(output[2].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[2].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[2].number.WholeNum() == 2, true);
			Assert::AreEqual<bool>(output[2].number.Numerator() == 4, true);
			Assert::AreEqual<bool>(output[2].number.Denominator() == 5, true);

			Assert::AreEqual<bool>(output[3].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[4].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[4].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[4].number.WholeNum() == 1, true);
			Assert::AreEqual<bool>(output[4].number.Numerator() == 2, true);
			Assert::AreEqual<bool>(output[4].number.Denominator() == 10, true);

			Assert::AreEqual<bool>(output[5].type == Formula::FormulaElementType::multiply, true);

			Assert::AreEqual<bool>(output[6].type == Formula::FormulaElementType::open_bracket, true);

			Assert::AreEqual<bool>(output[7].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[7].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[7].number.WholeNum() == 2, true);
			Assert::AreEqual<bool>(output[7].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[8].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(output[9].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[9].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[9].number.WholeNum() == 0, true);
			Assert::AreEqual<bool>(output[9].number.Numerator() == 4, true);
			Assert::AreEqual<bool>(output[9].number.Denominator() == 5, true);

			Assert::AreEqual<bool>(output[10].type == Formula::FormulaElementType::subtract, true);

			Assert::AreEqual<bool>(output[11].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[11].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[11].number.WholeNum() == 18, true);
			Assert::AreEqual<bool>(output[11].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[12].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[13].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[13].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[13].number.WholeNum() == 3, true);
			Assert::AreEqual<bool>(output[13].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[14].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[15].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[15].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[15].number.WholeNum() == 4, true);
			Assert::AreEqual<bool>(output[15].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[16].type == Formula::FormulaElementType::multiply, true);

			Assert::AreEqual<bool>(output[17].type == Formula::FormulaElementType::open_bracket, true);

			Assert::AreEqual<bool>(output[18].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[18].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[18].number.WholeNum() == 6, true);
			Assert::AreEqual<bool>(output[18].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[19].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(output[20].type == Formula::FormulaElementType::open_bracket, true);

			Assert::AreEqual<bool>(output[21].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[21].number.Sign() == -1, true);
			Assert::AreEqual<bool>(output[21].number.WholeNum() == 8, true);
			Assert::AreEqual<bool>(output[21].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[22].type == Formula::FormulaElementType::subtract, true);

			Assert::AreEqual<bool>(output[23].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[23].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[23].number.WholeNum() == 1, true);
			Assert::AreEqual<bool>(output[23].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[24].type == Formula::FormulaElementType::close_bracket, true);
			Assert::AreEqual<bool>(output[25].type == Formula::FormulaElementType::close_bracket, true);
			Assert::AreEqual<bool>(output[26].type == Formula::FormulaElementType::close_bracket, true);

			Assert::AreEqual<bool>(output.size() == 27, true);
		}

		TEST_METHOD(TextToFormula_4ConsecutiveDivide)
		{
			Formula::FormulaHelper helper;
			string formula = "0.123/4/5/6";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);

			vector<Formula::FormulaElement> output;
			b = helper.ItemsToFormula(items, output);
			Assert::AreEqual<bool>(b, true);

			Assert::AreEqual<bool>(output[0].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[0].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[0].number.WholeNum() == 0, true);
			Assert::AreEqual<bool>(output[0].number.Numerator() == 123, true);
			Assert::AreEqual<bool>(output[0].number.Denominator() == 1000, true);

			Assert::AreEqual<bool>(output[1].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[2].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[2].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[2].number.WholeNum() == 4, true);
			Assert::AreEqual<bool>(output[2].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[3].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[4].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[4].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[4].number.WholeNum() == 5, true);
			Assert::AreEqual<bool>(output[4].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[5].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[6].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[6].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[6].number.WholeNum() == 6, true);
			Assert::AreEqual<bool>(output[6].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output.size() == 7, true);
		}

		TEST_METHOD(TextToFormula_DivideAFractionWithWholeNumberNotInBracket)
		{
			Formula::FormulaHelper helper;
			string formula = "3/4 5/6";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);

			vector<Formula::FormulaElement> output;
			b = helper.ItemsToFormula(items, output);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(output[0].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[0].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[0].number.WholeNum() == 0, true);
			Assert::AreEqual<bool>(output[0].number.Numerator() == 3, true);
			Assert::AreEqual<bool>(output[0].number.Denominator() == 4, true);

			Assert::AreEqual<bool>(output[1].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[1].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[1].number.WholeNum() == 0, true);
			Assert::AreEqual<bool>(output[1].number.Numerator() == 5, true);
			Assert::AreEqual<bool>(output[1].number.Denominator() == 6, true);

			Assert::AreEqual<bool>(output.size() == 2, true);
		}

		TEST_METHOD(TextToFormula_DivideAFractionWithWholeNumberInBracket)
		{
			Formula::FormulaHelper helper;
			string formula = "3/(4 5/6)";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);

			vector<Formula::FormulaElement> output;
			b = helper.ItemsToFormula(items, output);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(output[0].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[0].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[0].number.WholeNum() == 3, true);
			Assert::AreEqual<bool>(output[0].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(output[1].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(output[2].type == Formula::FormulaElementType::open_bracket, true);

			Assert::AreEqual<bool>(output[3].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(output[3].number.Sign() == 1, true);
			Assert::AreEqual<bool>(output[3].number.WholeNum() == 4, true);
			Assert::AreEqual<bool>(output[3].number.Numerator() == 5, true);
			Assert::AreEqual<bool>(output[3].number.Denominator() == 6, true);

			Assert::AreEqual<bool>(output[4].type == Formula::FormulaElementType::close_bracket, true);

			Assert::AreEqual<bool>(output.size() == 5, true);
		}

		TEST_METHOD(TextToFormula_ConstructFormulaStack1)
		{
			Formula::FormulaHelper helper;
			string formula = "[(1 + 2*3 + 4) * 5 + 6] / 7";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);
			vector<Formula::FormulaElement> formulaElts;
			b = helper.ItemsToFormula(items, formulaElts);
			
			vector<Formula::FormulaElement> formulaStack;
			b = helper.ConstructFormulaStack(formulaElts, formulaStack);
			Assert::AreEqual<bool>(b, true);

			Assert::AreEqual<bool>(formulaStack[0].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[0].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[0].number.WholeNum() == 1, true);
			Assert::AreEqual<bool>(formulaStack[0].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[1].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[1].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[1].number.WholeNum() == 2, true);
			Assert::AreEqual<bool>(formulaStack[1].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[2].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[2].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[2].number.WholeNum() == 3, true);
			Assert::AreEqual<bool>(formulaStack[2].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[3].type == Formula::FormulaElementType::multiply, true);

			Assert::AreEqual<bool>(formulaStack[4].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(formulaStack[5].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[5].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[5].number.WholeNum() == 4, true);
			Assert::AreEqual<bool>(formulaStack[5].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[6].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(formulaStack[7].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[7].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[7].number.WholeNum() == 5, true);
			Assert::AreEqual<bool>(formulaStack[7].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[8].type == Formula::FormulaElementType::multiply, true);

			Assert::AreEqual<bool>(formulaStack[9].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[9].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[9].number.WholeNum() == 6, true);
			Assert::AreEqual<bool>(formulaStack[9].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[10].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(formulaStack[11].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[11].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[11].number.WholeNum() == 7, true);
			Assert::AreEqual<bool>(formulaStack[11].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[12].type == Formula::FormulaElementType::divide, true);

			Assert::AreEqual<bool>(formulaStack.size() == 13, true);
		}

		TEST_METHOD(TextToFormula_ConstructFormulaStack2)
		{
			Formula::FormulaHelper helper;
			string formula = "1 + [(3-4+8)*5] / (6+7)";
			vector<Formula::Item> items;
			bool b = helper.Text2Items(formula, items);
			Assert::AreEqual<bool>(b, true);
			b = helper.CheckBracketSanity(items);
			Assert::AreEqual<bool>(b, true);
			vector<Formula::FormulaElement> formulaElts;
			b = helper.ItemsToFormula(items, formulaElts);

			vector<Formula::FormulaElement> formulaStack;
			b = helper.ConstructFormulaStack(formulaElts, formulaStack);
			Assert::AreEqual<bool>(b, true);

			Assert::AreEqual<bool>(formulaStack[0].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[0].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[0].number.WholeNum() == 1, true);
			Assert::AreEqual<bool>(formulaStack[0].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[1].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[1].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[1].number.WholeNum() == 3, true);
			Assert::AreEqual<bool>(formulaStack[1].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[2].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[2].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[2].number.WholeNum() == 4, true);
			Assert::AreEqual<bool>(formulaStack[2].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[3].type == Formula::FormulaElementType::subtract, true);

			Assert::AreEqual<bool>(formulaStack[4].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[4].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[4].number.WholeNum() == 8, true);
			Assert::AreEqual<bool>(formulaStack[4].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[5].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(formulaStack[6].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[6].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[6].number.WholeNum() == 5, true);
			Assert::AreEqual<bool>(formulaStack[6].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[7].type == Formula::FormulaElementType::multiply, true);

			Assert::AreEqual<bool>(formulaStack[8].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[8].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[8].number.WholeNum() == 6, true);
			Assert::AreEqual<bool>(formulaStack[8].number.Numerator() == 0, true);
		
			Assert::AreEqual<bool>(formulaStack[9].type == Formula::FormulaElementType::number, true);
			Assert::AreEqual<bool>(formulaStack[9].number.Sign() == 1, true);
			Assert::AreEqual<bool>(formulaStack[9].number.WholeNum() == 7, true);
			Assert::AreEqual<bool>(formulaStack[9].number.Numerator() == 0, true);

			Assert::AreEqual<bool>(formulaStack[10].type == Formula::FormulaElementType::add, true);
			Assert::AreEqual<bool>(formulaStack[11].type == Formula::FormulaElementType::divide, true);
			Assert::AreEqual<bool>(formulaStack[12].type == Formula::FormulaElementType::add, true);

			Assert::AreEqual<bool>(formulaStack.size() == 13, true);
		}

	};
}