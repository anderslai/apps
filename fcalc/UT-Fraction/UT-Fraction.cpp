#include "pch.h"
#include "CppUnitTest.h"
#include "../fcalc/Fraction.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UTFraction
{
	TEST_CLASS(UTFraction)
	{
	public:
		
		TEST_METHOD(FractionSimplification_FractionWithWholeNumber)
		{
			Fraction fraction(2, 756, 3960);
			fraction.Simplify();
			Assert::AreEqual<long long>(fraction.WholeNum(), 2);
			Assert::AreEqual<long long>(fraction.Numerator(), 21);
			Assert::AreEqual<long long>(fraction.Denominator(), 110);
			Assert::AreEqual<long long>(fraction.Sign(), 1);
		}

		TEST_METHOD(FractionSimplification_ImproperFraction)
		{
			Fraction fraction(0, 3960, 756);
			fraction.Simplify();
			Assert::AreEqual<long long>(fraction.WholeNum(), 5);
			Assert::AreEqual<long long>(fraction.Numerator(), 5);
			Assert::AreEqual<long long>(fraction.Denominator(), 21);
			Assert::AreEqual<long long>(fraction.Sign(), 1);
		}

		TEST_METHOD(FractionSimplification_ZeroDenominator)
		{
			auto f = []() { Fraction f1(1, 1, 0); };
			Assert::ExpectException<std::invalid_argument>(f);
			//wchar_t message[100];
			//wcscpy_s(message, 99, L"zero denominator");
			Assert::ExpectException<std::invalid_argument>(f, L"zero denominator");
		}

		TEST_METHOD(FractionConstructionFromDecimal1)
		{
			Fraction fraction("1.23");
			fraction.Simplify();
			Assert::AreEqual<long long>(fraction.WholeNum(), 1);
			Assert::AreEqual<long long>(fraction.Numerator(), 23);
			Assert::AreEqual<long long>(fraction.Denominator(), 100);
			Assert::AreEqual<long long>(fraction.Sign(), 1);
		}

		TEST_METHOD(FractionConstructionFromDecimal2)
		{
			Fraction fraction("-0.34");
			fraction.Simplify();
			Assert::AreEqual<long long>(fraction.WholeNum(), 0);
			Assert::AreEqual<long long>(fraction.Numerator(), 17);
			Assert::AreEqual<long long>(fraction.Denominator(), 50);
			Assert::AreEqual<long long>(fraction.Sign(), -1);
		}

		TEST_METHOD(FractionConstructionFromDecimal3)
		{
			Fraction fraction("+1.00120");
			fraction.Simplify();
			Assert::AreEqual<long long>(fraction.WholeNum(), 1);
			Assert::AreEqual<long long>(fraction.Numerator(), 3);
			Assert::AreEqual<long long>(fraction.Denominator(), 2500);
			Assert::AreEqual<long long>(fraction.Sign(), 1);
		}

		TEST_METHOD(FractionConstructionFromDecimal4)
		{
			Fraction fraction("0.00000000");
			fraction.Simplify();
			Assert::AreEqual<long long>(fraction.WholeNum(), 0);
			Assert::AreEqual<long long>(fraction.Numerator(), 0);
			Assert::AreEqual<long long>(fraction.Sign(), 1);
		}

		TEST_METHOD(FractionConstructionFromDecimal_MoreThanOneDecimalPoint)
		{
			auto f = []() {Fraction fraction("1.0000.0000"); };
			Assert::ExpectException<std::invalid_argument>(f);			
		}

		TEST_METHOD(FractionConstructionFromDecimal_InvalidDigit1)
		{
			auto f = []() {Fraction fraction("1.0000A0000"); };
			Assert::ExpectException<std::invalid_argument>(f);
		}

		TEST_METHOD(FractionConstructionFromDecimal_InvalidDigit2)
		{
			auto f = []() {Fraction fraction("w.00000000"); };
			Assert::ExpectException<std::invalid_argument>(f);
		}

		TEST_METHOD(FractionConstructionFromDecimal_InvalidDigit3)
		{
			auto f = []() {Fraction fraction("++1.234"); };
			Assert::ExpectException<std::invalid_argument>(f);
		}

		TEST_METHOD(FractionAddition_TwoWholeNumber_PP)
		{
			Fraction f1(2, 0, 1);
			Fraction f2(1, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 3);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_TwoWholeNumber_NN)
		{
			Fraction f1(-123, 0, 1);
			Fraction f2(-456, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 579);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_TwoWholeNumber_PN)
		{
			Fraction f1(123, 0, 1);
			Fraction f2(-23, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 100);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_TwoWholeNumber_NP)
		{
			Fraction f1(-123, 0, 1);
			Fraction f2(23, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 100);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}


		TEST_METHOD(FractionAddition_OneWholeNumberLeft_PP)
		{
			Fraction f1(2, 0, 1);
			Fraction f2(1, 1, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 3);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberLeft_PN_PosResult)
		{
			Fraction f1(2, 0, 1);
			Fraction f2(-1, 1, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberLeft_NP_PosResult)
		{
			Fraction f1(-2, 0, 1);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberLeft_NP_NegResult)
		{
			Fraction f1(-2, 0, 1);
			Fraction f2(1, 1, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberLeft_PN_NegResult)
		{
			Fraction f1(2, 0, 1);
			Fraction f2(-2, 1, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberLeft_NN)
		{
			Fraction f1(-2, 0, 1);
			Fraction f2(-2, 1, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 4);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberRight_PP)
		{
			Fraction f1(1, 1, 3);
			Fraction f2(2, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 3);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberRight_PN_PosResult)
		{
			Fraction f1(3, 1, 3);
			Fraction f2(-2, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberRight_PN_NegResult)
		{
			Fraction f1(1, 1, 3);
			Fraction f2(-2, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberRight_NP_PosResult)
		{
			Fraction f1(-1, 1, 3);
			Fraction f2(2, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberRight_NP_NegResult)
		{
			Fraction f1(-3, 1, 3);
			Fraction f2(2, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_OneWholeNumberRight_NN)
		{
			Fraction f1(-1, 1, 3);
			Fraction f2(-2, 0, 1);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 3);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_PP)
		{
			Fraction f1(1, 9914, 15435);	// 3, 3, 5, 7, 7, 7
			Fraction f2(2, 509, 8250);		// 2, 3, 5, 5, 5, 11
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 3);
			Assert::AreEqual<long long>(f3.Numerator(),   5976461);
			Assert::AreEqual<long long>(f3.Denominator(), 8489250);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_PN_PosResult)
		{
			Fraction f1(2, 2, 5);
			Fraction f2(1, 3, -8);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 40);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_PN_NegResult)
		{
			Fraction f1(2, 2, 5);
			Fraction f2(2, 4, -8);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 10);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_NP_PosResult)
		{
			Fraction f1(1, 3, -8);
			Fraction f2(2, 2, 5);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 40);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_NP_NegResult)
		{
			Fraction f1(2, 4, -8);
			Fraction f2(2, 2, 5);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 10);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_NN)
		{
			Fraction f1(-1, 9914, 15435);	// 3, 3, 5, 7, 7, 7
			Fraction f2(2, -509, 8250);		// 2, 3, 5, 5, 5, 11
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 3);
			Assert::AreEqual<long long>(f3.Numerator(), 5976461);
			Assert::AreEqual<long long>(f3.Denominator(), 8489250);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionAddition_TwoFractions_ZeroResult)
		{
			//Fraction f1(2, 509, 8250);
			//Fraction f2(-2, 509, 8250);
			Fraction f1(1, 2, 3);
			Fraction f2(-1, 2, 3);
			Fraction f3 = f1 + f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}


		TEST_METHOD(FractionSubtraction_TwoWholeNumber_PP_PosResult)
		{
			Fraction f1(2);
			Fraction f2(1);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_TwoWholeNumber_PP_NegResult)
		{
			Fraction f1(1);
			Fraction f2(2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreNotEqual<long long>(f3.Denominator(), 0);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_TwoWholeNumber_NN_PosResult)
		{
			Fraction f1(-5);
			Fraction f2(-7);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreNotEqual<long long>(f3.Denominator(), 0);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_TwoWholeNumber_NN_NegResult)
		{
			Fraction f1(-5);
			Fraction f2(-1);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 4);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreNotEqual<long long>(f3.Denominator(), 0);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_TwoWholeNumber_PN)
		{
			Fraction f1(5);
			Fraction f2(-1);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 6);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreNotEqual<long long>(f3.Denominator(), 0);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_TwoWholeNumber_NP)
		{
			Fraction f1(-5);
			Fraction f2(1);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 6);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreNotEqual<long long>(f3.Denominator(), 0);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_WholeNum_Decimal)
		{
			Fraction f1(4);
			Fraction f2("5.2");
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 5);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_Decimal_Decimal)
		{
			Fraction f1("0.001");
			Fraction f2("5.2");
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 5);
			Assert::AreEqual<long long>(f3.Numerator(), 199);
			Assert::AreEqual<long long>(f3.Denominator(), 1000);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberLeft_PP_PosResult)
		{
			Fraction f1(2);
			Fraction f2(1, 1, 3);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberLeft_PP_NegResult)
		{
			Fraction f1(2);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberLeft_NN_NegResult)
		{
			Fraction f1(-2);
			Fraction f2(-1, 1, 3);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberLeft_NN_PosResult)
		{
			Fraction f1(-2);
			Fraction f2(-2, 1, 3);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberLeft_PN)
		{
			Fraction f1(2);
			Fraction f2(-2, 1, 3);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 4);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberLeft_NP)
		{
			Fraction f1(-3);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 5);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberRight_PP_PosResult)
		{
			Fraction f1(3, 1, 4);
			Fraction f2(2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberRight_PP_NegResult)
		{
			Fraction f1(1, 1, 4);
			Fraction f2(2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberRight_NN_PosResult)
		{
			Fraction f1(-1, 1, 4);
			Fraction f2(-2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberRight_NN_NegResult)
		{
			Fraction f1(-2, 3, 4);
			Fraction f2(-2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberRight_PN)
		{
			Fraction f1(2, 3, 4);
			Fraction f2(-2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 4);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_OneWholeNumberRight_NP)
		{
			Fraction f1(-2, 3, 4);
			Fraction f2(2);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 4);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_TwoFraction_PP_PosResult)
		{
			Fraction f1(2, 2, 5);
			Fraction f2(0, 1, 4);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 20);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_TwoFraction_PP_NegResult)
		{
			Fraction f1(2, 2, 5);
			Fraction f2(3, 1, 4);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 17);
			Assert::AreEqual<long long>(f3.Denominator(), 20);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_TwoFraction_NN_PosResult)
		{
			Fraction f1(0, -1, 4);
			Fraction f2(-2, 2, 5);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 20);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_TwoFraction_NN_NegResult)
		{
			Fraction f1(-2, 3, 4);
			Fraction f2(-2, 2, 5);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 7);
			Assert::AreEqual<long long>(f3.Denominator(), 20);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionSubtraction_TwoFraction_PN)
		{
			Fraction f1(2, 3, 4);
			Fraction f2(-2, 2, 5);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 5);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 20);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionSubtraction_TwoFraction_NP)
		{
			Fraction f1(-2, 3, 4);
			Fraction f2(2, 2, 5);
			Fraction f3 = f1 - f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 5);
			Assert::AreEqual<long long>(f3.Numerator(), 3);
			Assert::AreEqual<long long>(f3.Denominator(), 20);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}



		TEST_METHOD(FractionMultiplication_OneWholeNumberLeft_PP)
		{
			Fraction f1(2);
			Fraction f2(1, 1, 3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberLeft_PN)
		{
			Fraction f1(2);
			Fraction f2(-1, 1, 3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberLeft_NP)
		{
			Fraction f1(-3);
			Fraction f2(2, 3, 7);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 7);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberLeft_NN)
		{
			Fraction f1(-3);
			Fraction f2(-2, 3, 7);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 7);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberRight_PP)
		{
			Fraction f1(1, 1, 3);
			Fraction f2(2);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberRight_PN)
		{
			Fraction f2(1, 1, 3);
			Fraction f1(-2);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 2);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 3);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberRight_NP)
		{
			Fraction f1(-2, 1, 3);
			Fraction f2(3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionMultiplication_OneWholeNumberRight_NN)
		{
			Fraction f1(-2, 1, 3);
			Fraction f2(-3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreEqual<long long>(f3.Denominator(), 1);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionMultiplication_TwoFractions_PP)
		{
			Fraction f1(3, 1, 6);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 7);
			Assert::AreEqual<long long>(f3.Denominator(), 18);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionMultiplication_TwoFractions_PN)
		{
			Fraction f1(3, 1, 6);
			Fraction f2(-2, 1, 3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 7);
			Assert::AreEqual<long long>(f3.Denominator(), 18);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionMultiplication_TwoFractions_NP)
		{
			Fraction f1(-3, 1, 6);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 7);
			Assert::AreEqual<long long>(f3.Denominator(), 18);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionMultiplication_TwoFractions_NN)
		{
			Fraction f1(-3, 1, 6);
			Fraction f2(2, 1, -3);
			Fraction f3 = f1 * f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 7);
			Assert::AreEqual<long long>(f3.Denominator(), 18);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_byZero)
		{
			Fraction f1(3, 1, 6);
			Fraction f2(0);
			bool catchExcept = false;
			try
			{
				Fraction f3 = f1 / f2;
			}
			catch (overflow_error& e)
			{
				string error(e.what());
				if (error == "division by zero")
				{
					catchExcept = true;
				}
			}
			Assert::AreEqual(catchExcept, true);
		}

		TEST_METHOD(FractionDivision_TwoWholeNum_PP)
		{
			Fraction f1(3);
			Fraction f2(2);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 2);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_TwoWholeNum_PN)
		{
			Fraction f1(21);
			Fraction f2(-3);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 7);
			Assert::AreEqual<long long>(f3.Numerator(), 0);
			Assert::AreNotEqual<long long>(f3.Denominator(), 0);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_TwoWholeNum_NP)
		{
			Fraction f1(-4);
			Fraction f2(16);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 4);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_TwoWholeNum_NN)
		{
			Fraction f1(-1234);
			Fraction f2(-14);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 88);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 7);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_TwoFractions_PP)
		{
			Fraction f1(3, 1, 6);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 5);
			Assert::AreEqual<long long>(f3.Denominator(), 14);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_TwoFractions_PN)
		{
			Fraction f1(3, 1, 6);
			Fraction f2(2, -1, 3);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 5);
			Assert::AreEqual<long long>(f3.Denominator(), 14);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_TwoFractions_NP)
		{
			Fraction f1(0, -18, 6);
			Fraction f2(2, 1, 3);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 2);
			Assert::AreEqual<long long>(f3.Denominator(), 7);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_TwoFractions_NN)
		{
			Fraction f1(3, -1, 6);
			Fraction f2(2, 1, -3);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 1);
			Assert::AreEqual<long long>(f3.Numerator(), 5);
			Assert::AreEqual<long long>(f3.Denominator(), 14);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_FractionDividedByWholeNum_PP)
		{
			Fraction f1(381, 8, 27);
			Fraction f2(2);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 190);
			Assert::AreEqual<long long>(f3.Numerator(), 35);
			Assert::AreEqual<long long>(f3.Denominator(), 54);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_FractionDividedByWholeNum_PN)
		{
			Fraction f1(381, 8, 27);
			Fraction f2(-2);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 190);
			Assert::AreEqual<long long>(f3.Numerator(), 35);
			Assert::AreEqual<long long>(f3.Denominator(), 54);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_FractionDividedByWholeNum_NP)
		{
			Fraction f1(-381, -8, -27);
			Fraction f2(2);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 190);
			Assert::AreEqual<long long>(f3.Numerator(), 35);
			Assert::AreEqual<long long>(f3.Denominator(), 54);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_FractionDividedByWholeNum_NN)
		{
			Fraction f1(381, -8, -27);
			Fraction f2(-2);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 190);
			Assert::AreEqual<long long>(f3.Numerator(), 35);
			Assert::AreEqual<long long>(f3.Denominator(), 54);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_WholeNumDividedByFraction_PP)
		{
			Fraction f1(2);
			Fraction f2(3, 5, 12);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 24);
			Assert::AreEqual<long long>(f3.Denominator(), 41);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_WholeNumDividedByFraction_PN)
		{
			Fraction f1(2);
			Fraction f2(3, 5, -12);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 24);
			Assert::AreEqual<long long>(f3.Denominator(), 41);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_WholeNumDividedByFraction_NP)
		{
			Fraction f1(-2);
			Fraction f2(3, 5, 12);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 24);
			Assert::AreEqual<long long>(f3.Denominator(), 41);
			Assert::AreEqual<long long>(f3.Sign(), -1);
		}

		TEST_METHOD(FractionDivision_WholeNumDividedByFraction_NN)
		{
			Fraction f1(-2);
			Fraction f2(-3, 5, -12);
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 0);
			Assert::AreEqual<long long>(f3.Numerator(), 24);
			Assert::AreEqual<long long>(f3.Denominator(), 41);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

		TEST_METHOD(FractionDivision_TwoDecimals)
		{
			Fraction f1("5.02");
			Fraction f2("0.04");
			Fraction f3 = f1 / f2;
			Assert::AreEqual<long long>(f3.WholeNum(), 125);
			Assert::AreEqual<long long>(f3.Numerator(), 1);
			Assert::AreEqual<long long>(f3.Denominator(), 2);
			Assert::AreEqual<long long>(f3.Sign(), 1);
		}

	};
}
