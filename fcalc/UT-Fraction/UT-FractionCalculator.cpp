#include "pch.h"
#include "CppUnitTest.h"
#include "../fcalc/FractionCalculator.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UTFractionCalculator
{
	TEST_CLASS(UTFractionCalculator)
	{
	public:

		TEST_METHOD(TestMethod_PlusPlusPlus)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("1+2+3", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == 1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 6, true);
			Assert::AreEqual<bool>(f.Numerator() == 0, true);
		}

		TEST_METHOD(TestMethod_MinusMinusMinusMinus)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("-1-2-3-4", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == -1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 10, true);
			Assert::AreEqual<bool>(f.Numerator() == 0, true);
		}

		TEST_METHOD(TestMethod_Calculate_RandomFormula)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("1 2/3 + 4 5/6 * 7 8/9 * 10 / 2 / 3 / (4 - 5.2 - 0.1*10*2/(1+4))", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == -1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 38, true);
			Assert::AreEqual<bool>(f.Numerator() == 67, true);
			Assert::AreEqual<bool>(f.Denominator() == 1296, true);
		}

		TEST_METHOD(TestMethod_ArithmeticWithNumberInsideBracket)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("[4 / 5 - ({ 6 })] * (7 + 8)", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == -1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 78, true);
			Assert::AreEqual<bool>(f.Numerator() == 0, true);
		}

		TEST_METHOD(TestMethod_Calculate_DivideInvolvingBrackets)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("[(1+2)+3] / (6.11 + 5.89 - 0.00)", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == 1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 0, true);
			Assert::AreEqual<bool>(f.Numerator() == 1, true);
			Assert::AreEqual<bool>(f.Denominator() == 2, true);
		}

		TEST_METHOD(TestMethod_Calculate_DivideInvolvingBracketsInNumerator)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("[(1+2)+3] / 0.02", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == 1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 300, true);
			Assert::AreEqual<bool>(f.Numerator() == 0, true);
			Assert::AreEqual<bool>(f.Denominator() != 0, true);
		}

		TEST_METHOD(TestMethod_Calculate_DivideInvolvingBracketsInDenominator)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("12 / {[(1+2)+(3-0.5)*2] / 2}", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == 1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 3, true);
			Assert::AreEqual<bool>(f.Numerator() == 0, true);
			Assert::AreEqual<bool>(f.Denominator() != 0, true);
		}

		TEST_METHOD(TestMethod_Calculate_OperatorPrecedence)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("-1+2-3*4/5/6/7*8+9/10+11*0.1-12/1+0.2*10*0.01", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == -1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 9, true);
			Assert::AreEqual<bool>(f.Numerator() == 153, true);
			Assert::AreEqual<bool>(f.Denominator() == 350, true);
		}

		TEST_METHOD(TestMethod_SingleNumberOnly1)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("52/32", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == 1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 1, true);
			Assert::AreEqual<bool>(f.Numerator() == 5, true);
			Assert::AreEqual<bool>(f.Denominator() == 8, true);
		}

		TEST_METHOD(TestMethod_SingleNumberOnly2)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("(-4/12)", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == -1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 0, true);
			Assert::AreEqual<bool>(f.Numerator() == 1, true);
			Assert::AreEqual<bool>(f.Denominator() == 3, true);
		}

		TEST_METHOD(TestMethod_SingleNumberOnly3)
		{
			FractionCalculator fcalc;
			Fraction f(0);
			bool b = fcalc.Calculate("{[(-1 52/32)]}", f);
			Assert::AreEqual<bool>(b, true);
			Assert::AreEqual<bool>(f.Sign() == -1, true);
			Assert::AreEqual<bool>(f.WholeNum() == 2, true);
			Assert::AreEqual<bool>(f.Numerator() == 5, true);
			Assert::AreEqual<bool>(f.Denominator() == 8, true);
		}
	};
}