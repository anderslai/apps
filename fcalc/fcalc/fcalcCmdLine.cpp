#include "fcalcCmdLine.h"
#include <map>
#include <iostream>
#include <filesystem>
namespace fs = std::filesystem;

fcalcCmdLine::fcalcCmdLine(int argc, char* argv[]) : 
    usage("fcalc [-d] [-v] { [-f formula_file] or [formula] }")
{
    if ((argc == 2) && (_strcmpi("-h", argv[1]) == 0))
    {   
        string message = usage;
        message += "\n";
        message += "-d --> display result in decimal value\n";
        message += "-v --> display result format in [formula] = [result]\n";
        message += "-f --> formula stored in file with one line per one formula.\n";
        message += "If no formula supplied at command line or via file, take formula from stdin\n";
        message += "Examples:\n";
        message += "> fcalc ( {[(1+2)*3 + 4] - 5} * 6 / 7 + 8 9/10 + 0.1 ) / 3\n";
        message += "> 5 2/7\n";
        message += "> fcalc -v -d ( {[(1+2)*3 + 4] - 5} * 6 / 7 + 8 9/10 + 0.1 ) / 3\n";
        message += "> ( {[(1+2)*3 + 4] - 5} * 6 / 7 + 8 9/10 + 0.1 ) / 3 = 5.285714285714286";
        throw message;
    }

    enum class Status
    {
        expect_option_or_formula,
        expect_file,
    };
    Status state = Status::expect_option_or_formula;

    outputInFraction = true;
    verbose = false;
    inputSource = InputSource::standard_input;

    map<string, bool> options;
    options["-f"] = false;
    options["-d"] = false;
    options["-v"] = false;

    const string switch_d = "-d";
    const string switch_f = "-f";
    const string switch_v = "-v";

    formula = "";
    bool foundFormula = false;

    for (int n = 1; n < argc; n++)
    {
        switch (state)
        {
            case Status::expect_option_or_formula:
            {
                if (switch_d.compare(argv[n]) == 0)
                {
                    if (options[switch_d] == false)
                    {
                        outputInFraction = false;
                        options[switch_d] = true;
                        if (formula != "")
                        {
                            foundFormula = true;
                        }
                    }
                    else
                    {
                        throw string("Duplicate option -d. Usage: " + usage);
                    }
                }
                else if (switch_v.compare(argv[n]) == 0)
                {
                    if (options[switch_v] == false)
                    {
                        verbose = true;
                        options[switch_v] = true;
                        if (formula != "")
                        {
                            foundFormula = true;
                        }
                    }
                    else
                    {
                        throw string("Duplicate option -v. Usage: " + usage);
                    }
                }
                else if (switch_f.compare(argv[n]) == 0) //(argv[n] == "-f")
                {
                    if (options[switch_f] == false)
                    {
                        if (n < argc - 1)
                        {
                            state = Status::expect_file;
                            options[switch_f] = true;
                        }
                        else
                        {
                            throw string("Missing formula file. Usage: " + usage);
                        }
                    }
                    else
                    {
                        throw string("Duplicate option -f. Usage: " + usage);
                    }
                }
                else
                {
                    if (foundFormula != true)
                    {
                        formula.append(argv[n]);
                        if (n != argc - 1)
                        {
                            formula.append(" ");
                        }
                    }
                    else
                    {
                        throw string("Formula appearing more than once. Usage : " + usage);
                    }
                }
                break;
            }
            case Status::expect_file:
            {
                std::filesystem::path p = fs::current_path();
                string s = p.string();
                ifs.open(argv[n], ifstream::in);
                if (ifs.is_open())
                {
                    state = Status::expect_option_or_formula;
                    inputSource = InputSource::file;
                    if (formula != "")
                    {
                        foundFormula = true;
                    }
                }
                else
                {
                    throw string("Failed to open file");
                }
                break;
            }
        }
    }

    if (formula != "")
    {
        inputSource = InputSource::command_line;
    }
}

fcalcCmdLine::~fcalcCmdLine()
{
    if (ifs.is_open())
    {
        ifs.close();
    }
}

bool fcalcCmdLine::OutputInFraction() const
{
    return outputInFraction;
}

bool fcalcCmdLine::Verbose() const
{
    return verbose;
}

bool fcalcCmdLine::InputFromStdin() const
{
    return (inputSource == InputSource::standard_input);
}

string fcalcCmdLine::GetNextTextFormula()
{
    if (inputSource == InputSource::command_line)
    {
        string s = formula;
        formula = "";
        return s;
    }
    else if (inputSource == InputSource::file)
    {
        auto IsBlankLine = [](string text)
        {
            if (text == "")
            {
                return true;
            }
            else
            {
                for (auto c : text)
                {
                    if (!isspace(c))
                    {
                        return false;
                    }
                }
                return true;
            }
        };

        string text;
        while (getline(ifs, text))
        {
            if (!IsBlankLine(text))
            {
                formula = text;
                return formula;
            }
        }
        return "";
        //if (getline(ifs, text))
        //{
        //    formula = text;
        //    return formula;
        //}
        //else
        //{
        //    return "";
        //}
    }
    else if (inputSource == InputSource::standard_input)
    {
        string text;
        if (getline(cin, text))
        {
            formula = text;
            return formula;
        }
        else
        {
            return "";
        }
    }
    else
    {
        return "";
    }
}
