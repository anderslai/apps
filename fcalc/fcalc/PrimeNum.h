#pragma once
#include <vector>
#include <string>
using namespace std;

class PrimeNum
{
public:
    PrimeNum(void);

    bool IsPrime(long long number);
    vector<long long> FindPrimeFactors(long long number);

private:
    vector<long long> primes;
};