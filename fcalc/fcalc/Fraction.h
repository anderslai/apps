#pragma once
#include <map>
#include <vector>
#include <string>
using namespace std;

class Fraction
{
private:
    Fraction(long long wholeNum, long long fraction);
public:
    
    Fraction(void) = delete;
    // any portion negative --> whole value is negative
    // all portion positive --> whole value is positive
    Fraction(long long wholeNum);
    
    Fraction(long long wholeNum, long long numerator, long long denominator);
    Fraction(string decimal);

    Fraction operator+(const Fraction& rhs);
    Fraction operator-(const Fraction& rhs);
    Fraction operator*(const Fraction& rhs);
    Fraction operator/(const Fraction& rhs);
    
    long long WholeNum() const;
    long long Numerator() const;
    long long Denominator() const;
    int Sign() const;
    long long SignedWholeNum() const { return WholeNum() * Sign(); }
    long long SignedNumerator() const { return Numerator() * Sign(); }
    long long SignedDenominator() const { return Denominator() * Sign(); }
    double Decimal() const;

    void Simplify(void);

    long long GetLCM(const Fraction& f1, const Fraction& f2);

private:
    long long wholeNum;
    long long numerator;
    long long denominator;
    bool positive;

    long long GetDenominatorFor(long long number);
    map<long long, int> SummarizeFactors(vector<long long>& factors);
};
