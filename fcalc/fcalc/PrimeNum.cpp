#include "PrimeNum.h"
#include <algorithm>

PrimeNum::PrimeNum(void)
{
    primes = { 1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
}

bool PrimeNum::IsPrime(long long number)
{
    long long num = (number < 0) ? number * (-1) : number;
    
    // zero is not a prime
    if (num == 0)
    {
        return false;
    }

    // see if divisble by basic primes
    vector<long long> basic = { 2, 3, 5, 7 };
    for (auto b : basic)
    {
        if ((num % b) == 0)
        {
            return false;
        }
    }

    // make sure single digit exists in the list of primes
    vector<long long> single_digit_primes = { 1 };
    single_digit_primes.insert(single_digit_primes.end(), basic.begin(), basic.end());
    for (auto sdp : single_digit_primes)
    {
        auto it = find(primes.begin(), primes.end(), 1);
        if (it == primes.end())
        {
            primes.push_back(sdp);
        }
    }
    sort(primes.begin(), primes.end(), [](int i, int j) { return i < j;  });
    
    // see if it exists in existing list of primes
    auto it = find(primes.begin(), primes.end(), num);
    if (it != primes.end())
    {
        primes.push_back(num);
        return true;
    }

    long long t = 1;
    vector<long long> odds = { 1, 3, 7, 9 };
    while (true)
    {
        for (auto o : odds)
        {
            long long n = (10 * t) + o;
            if (n < num)
            {
                if ((num % n) == 0)
                {
                    return false;
                }
            }
            else
            {
                primes.push_back(num);
                return true;
            }
        }
        t++;
    }
    return true;
}

vector<long long> PrimeNum::FindPrimeFactors(long long number)
// if number < 0, it will be converted to > 0 values before finding the prime factors
{
    vector<long long> prime_factors;
    vector<long long> single_digit_primes = { 2, 3, 5, 7};
    //long long num = number;
    long long num = (number < 0) ? number * (-1) : number;
    if (num == 0)
    {
        return prime_factors;
    }
    for (auto p : single_digit_primes)
    {
        while ((num % p) == 0)
        {
            prime_factors.push_back(p);
            num = num / p;
        }
        if (num == 1)
        {
            return prime_factors;
        }
    }

    long long base = 1;
    vector<long long> odds = { 1, 3, 7, 9 };
    while (true)
    {
        for (auto o : odds)
        {
            long long n = (10 * base) + o;
            if ( (n <= num) && (IsPrime(n)) )
            {
                while ((num % n) == 0)
                {
                    prime_factors.push_back(n);
                    num = num / n;
                }
                if (num == 1)
                {
                    return prime_factors;
                }
            }
            if (n > num)
            {
                prime_factors.clear();
                return prime_factors;
            }
        }
        base++;
    }
    prime_factors.clear();
    return prime_factors;
}