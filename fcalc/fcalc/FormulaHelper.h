#pragma once
#include <string>
#include <vector>
#include "Fraction.h"
using namespace std;

// digits
// decimal_point
// divide
// plus
// minus
// multiply

namespace Formula
{
    enum class ItemType
    {
        digits,
        sign,
        open_bracket,
        close_bracket,
        decimal_number,
        math_operator
    };

    struct Item
    {
        ItemType type;
        string value;
    };

    enum class FormulaElementType
    {
        open_bracket,
        close_bracket,
        number,
        add,
        subtract,
        multiply,
        divide
    };

    struct FormulaElement
    {
        FormulaElementType type;
        union
        {
            Fraction number;
            int precedence;
        };
    };

    class FormulaHelper
    {
    public:
        bool Text2Items(string text, vector<Item>& items);
        bool CheckBracketSanity(const vector<Item>& items);
        bool ItemsToFormula(const vector<Item>& input, vector<FormulaElement>& output);
        bool ConstructFormulaStack(const vector<FormulaElement>& formula, vector<FormulaElement>& stack);

    private:
        static bool IsOpenBracket(char c);
        static bool IsCloseBracket(char c);
        static bool IsSign(char c);
        static bool IsDecimalPoint(char c);
        enum class OperatorStatus
        {
            yes,
            pending,
            no
        };
        static OperatorStatus IsValidOperator(char c, string current);
        static bool DecomposeStringDecimal(string decimal, long long& wholeNumber, long long& fraction);

        static bool IsOperator(const FormulaElement& elt);
    };
};