#include "FormulaHelper.h"
#include <ctype.h>
#include <stack>
#include <algorithm>
using namespace Formula;

bool FormulaHelper::Text2Items(string text, vector<Item>& items)
{
    enum class ParseState
    {
        start,
        expect_first_digit,
        expect_digit,
        expect_first_fractional_digit,
        expect_fractional_digit,
        middle
    };
    ParseState state = ParseState::start;
    string currentItem = "";

    for (auto c : text)
    {
        switch (state)
        {
        case ParseState::start:
        {
            if (isspace(c))
            {
                // no change in state, simply skip
            }
            else if (IsOpenBracket(c))
            {
                currentItem = string(1, c);
                items.push_back(Item{ ItemType::open_bracket, currentItem });
                currentItem = "";
                // no change in state
            }
            else if (isdigit(c))
            {
                currentItem = c;
                state = ParseState::expect_digit;
            }
            else if (IsSign(c))
            {
                currentItem = string(1, c);
                items.push_back(Item{ ItemType::sign, currentItem });
                state = ParseState::expect_first_digit;
                currentItem = "";
            }
            else
            {
                return false;   // invalid character encountered
            }
            break;
        }
        case ParseState::expect_first_digit:
        {
            if (isdigit(c))
            {
                currentItem = c;
                state = ParseState::expect_digit;
            }
            else if (isspace(c))
            {
                // no change
            }
            else
            {
                return false;
            }
            break;
        }
        case ParseState::expect_digit: 
        {
            if (isdigit(c))
            {
                currentItem += c;
            }
            else if (isspace(c))
            {
                items.push_back(Item{ ItemType::digits, currentItem });
                currentItem = "";
                state = ParseState::middle;
            }
            else if (IsOpenBracket(c))
            {
                items.push_back(Item{ ItemType::digits, currentItem });
                currentItem = c;
                items.push_back(Item{ ItemType::open_bracket, currentItem });
                state = ParseState::middle;
                currentItem = "";
            }
            else if (IsCloseBracket(c))
            {
                items.push_back(Item{ ItemType::digits, currentItem });
                currentItem = c;
                items.push_back(Item{ ItemType::close_bracket, currentItem });
                state = ParseState::middle;
                currentItem = "";
            }
            else if (IsDecimalPoint(c))
            {
                currentItem += c;
                state = ParseState::expect_first_fractional_digit;
            }
            else 
            {
                OperatorStatus s = IsValidOperator(c, currentItem);
                if (s == OperatorStatus::yes)
                {
                    items.push_back(Item{ ItemType::digits, currentItem});
                    currentItem = c;
                    items.push_back(Item{ ItemType::math_operator, currentItem });
                    state = ParseState::middle;
                    currentItem = "";
                }
                else
                {
                    return false;
                }
            }
            break;
        }
        case ParseState::expect_first_fractional_digit:
        {
            if (isdigit(c))
            {
                currentItem += c;
                state = ParseState::expect_fractional_digit;
            }
            else
            {
                return false;
            }
            break;
        }
        case ParseState::expect_fractional_digit:
        {
            if (isdigit(c))
            {
                currentItem += c;
            }
            else if (IsValidOperator(c, currentItem) == OperatorStatus::yes)
            {
                items.push_back(Item{ ItemType::decimal_number, currentItem });
                currentItem = c;
                items.push_back(Item{ ItemType::math_operator, currentItem });
                state = ParseState::middle;
                currentItem = "";
            }
            else if (IsOpenBracket(c))
            {
                items.push_back(Item{ ItemType::decimal_number, currentItem });
                currentItem = c;
                items.push_back(Item{ ItemType::open_bracket, currentItem });
                state = ParseState::middle;
                currentItem = "";
            }
            else if (IsCloseBracket(c))
            {
                items.push_back(Item{ ItemType::decimal_number, currentItem });
                currentItem = c;
                items.push_back(Item{ ItemType::close_bracket, currentItem });
                state = ParseState::middle;
                currentItem = "";
            }
            else if (isspace(c))
            {
                items.push_back(Item{ ItemType::decimal_number, currentItem });
                currentItem = "";
                state = ParseState::middle;
            }
            else if (IsDecimalPoint(c))
            {
                return false;
            }
            break;
        }
        case ParseState::middle:
        {
            OperatorStatus s = IsValidOperator(c, currentItem);
            if (s == OperatorStatus::yes)
            {
                currentItem = string(1, c);
                items.push_back(Item{ ItemType::math_operator, currentItem });
                currentItem = "";
                // no change in state
            }
            else if (IsCloseBracket(c))
            {
                currentItem = string(1, c);
                items.push_back(Item{ ItemType::close_bracket, currentItem });
                currentItem = "";
                // no change in state
            }
            else if (IsOpenBracket(c))
            {
                currentItem = string(1, c);
                items.push_back(Item{ ItemType::open_bracket, currentItem });
                state = ParseState::start;
                currentItem = "";
            }
            else if (isdigit(c))
            {
                currentItem = c;
                state = ParseState::expect_digit;
            }
            else if (isspace(c))
            {
                // no action
            }
            else
            {
                return false;
            }
            break;
        }
        default:
        {
            return false;
        }
        }
    }
    
    switch (state)
    {
    case ParseState::expect_digit:
    {
        items.push_back(Item{ ItemType::digits, currentItem });
        return true;
    }
    case ParseState::expect_fractional_digit:
    {
        items.push_back(Item{ ItemType::decimal_number, currentItem });
        return true;
    }
    case ParseState::start:
    case ParseState::middle:
    {
        if (IsOpenBracket(currentItem[0]))
        {
            items.push_back(Item{ ItemType::open_bracket, currentItem });
            return true;
        }
        if (IsCloseBracket(currentItem[0]))
        {
            items.push_back(Item{ ItemType::close_bracket, currentItem });
            return true;
        }
        if (IsValidOperator(currentItem[0], currentItem) == OperatorStatus::yes)
        {
            items.push_back(Item{ ItemType::math_operator, currentItem });
            return true;
        }
        if (isspace(currentItem[0]))
        {
            return true;
        }
        if (currentItem.size() == 0)
        {
            return true;
        }
    }
    default: break;
    }

    return false;
}

bool FormulaHelper::CheckBracketSanity(const vector<Item>& items)
{
    stack<string> brackets;
    for (auto item : items)
    {
        if (item.type == Formula::ItemType::open_bracket)
        {
            brackets.push(item.value);
        }
        else if (item.type == Formula::ItemType::close_bracket)
        {
            if (!brackets.empty())
            {
                string open = brackets.top();
                if (((item.value == ")") && (open == "(")) ||
                    ((item.value == "]") && (open == "[")) ||
                    ((item.value == "}") && (open == "{")))
                {
                    brackets.pop();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;   // too many close brackets
            }
        }
    }
    return (brackets.size() == 0);
}

bool FormulaHelper::ItemsToFormula(const vector<Item>& input, vector<FormulaElement>& output)
{
    size_t n = 0;
    while (n < input.size())
    {
        if (input[n].type == ItemType::open_bracket)
        {
            output.push_back(FormulaElement{ FormulaElementType::open_bracket, 0 });
            n++;
        }
        else if (input[n].type == ItemType::close_bracket)
        {
            output.push_back(FormulaElement{ FormulaElementType::close_bracket, 0 });
            n++;
        }
        else if (input[n].type == ItemType::sign)
        {
            if ((input.size() - n >= 5) &&
                (input[n + 1].type == ItemType::digits) &&
                (input[n + 2].type == ItemType::digits) &&
                (input[n + 3].type == ItemType::math_operator) && (input[n + 3].value == "/") &&
                (input[n + 4].type == ItemType::digits))
            {   // sign digits digits / digits
                int sign = (input[n].value == "-") ? -1 : 1;
                long long wholeNumber = stoll(input[n + 1].value) * sign;
                long long numerator = stoll(input[n + 2].value);
                long long denominator = stoll(input[n + 4].value);
                if (denominator == 0)
                {
                    return false;
                }
                output.push_back(FormulaElement{ FormulaElementType::number, Fraction(wholeNumber, numerator, denominator) });
                n += 5;
            }
            else if ( ((input.size() - n > 4) &&
                       (input[n + 1].type == ItemType::digits) &&
                       (input[n + 2].type == ItemType::math_operator) && (input[n + 2].value == "/") &&
                       (input[n + 3].type == ItemType::digits) &&
                       !((input[n + 4].type == ItemType::math_operator) && (input[n + 4].value == "/")) &&
                       !((n != 0) && (output.back().type == FormulaElementType::divide)) ) ||
                      ((input.size() - n == 4) &&
                       (input[n + 1].type == ItemType::digits) &&
                       (input[n + 2].type == ItemType::math_operator) && (input[n + 2].value == "/") &&
                       (input[n + 3].type == ItemType::digits) &&
                       !((n != 0) && (output.back().type == FormulaElementType::divide)) ) )
            {   // sign digits / digits, not followed or preceded by "/"
                int sign = (input[n].value == "-") ? -1 : 1;
                long long numerator = stoll(input[n + 1].value) * sign;
                long long denominator = stoll(input[n + 3].value);
                if (denominator == 0)
                {
                    return false;
                }
                output.push_back(FormulaElement{ FormulaElementType::number, Fraction(0, numerator, denominator) });
                n += 4;
            }
            else if ((input.size() - n >= 2) && (input[n + 1].type == ItemType::decimal_number))
            {   // sign decimal_number
                //int sign = (input[n].value == "-") ? -1 : 1;
                //long long wholeNumber = 0;
                //long long fraction = 0;
                //if (!DecomposeStringDecimal(input[n + 1].value, wholeNumber, fraction))
                //{
                //    return false;
                //}
                //output.push_back(FormulaElement{ FormulaElementType::number, Fraction(wholeNumber * sign, fraction) });
                try
                {
                    Fraction decNum(input[n].value + input[n + 1].value);
                    output.push_back(FormulaElement{ FormulaElementType::number, decNum });
                }
                catch (...)
                {
                    return false;
                }
                n += 2;
            }
            else if ((input.size() - n >= 2) && (input[n + 1].type == ItemType::digits))
            {
                int sign = (input[n].value == "-") ? -1 : 1;
                long long wholeNumber = stoll(input[n + 1].value) * sign;
                output.push_back(FormulaElement{ FormulaElementType::number, Fraction(wholeNumber) });
                n += 2;
            }
            else
            {
                return false;   // sign not followed by something valid
            }
        }
        else if (input[n].type == ItemType::digits)
        {
            if ((input.size() - n >= 4) &&
                (input[n + 1].type == ItemType::digits) &&
                (input[n + 2].type == ItemType::math_operator) && (input[n + 2].value == "/") &&
                (input[n + 3].type == ItemType::digits))
            {   //digits digits / digits
                long long wholeNumber = stoll(input[n].value);
                long long numerator = stoll(input[n + 1].value);
                long long denominator = stoll(input[n + 3].value);
                if (denominator == 0)
                {
                    return false;
                }
                output.push_back(FormulaElement{ FormulaElementType::number, Fraction(wholeNumber, numerator, denominator) });
                n += 4;
            }
            else if (((input.size() - n > 3) &&
                      (input[n + 1].type == ItemType::math_operator) && (input[n + 1].value == "/") &&
                      (input[n + 2].type == ItemType::digits) &&
                      !((input[n + 3].type == ItemType::math_operator) && (input[n + 3].value == "/")) &&
                      !((n != 0) && (output.back().type == FormulaElementType::divide)) ) ||
                     ((input.size() - n == 3) &&
                      (input[n + 1].type == ItemType::math_operator) && (input[n + 1].value == "/") &&
                      (input[n + 2].type == ItemType::digits) &&
                      !((n != 0) && (output.back().type == FormulaElementType::divide)) ))
            {   // digits / digits, not followed or preceded by "/"
                long long numerator = stoll(input[n].value);
                long long denominator = stoll(input[n + 2].value);
                if (denominator == 0)
                {
                    return false;
                }
                output.push_back(FormulaElement{ FormulaElementType::number, Fraction(0, numerator, denominator) });
                n += 3;
            }
            else
            {
                long long number = stoll(input[n].value);
                output.push_back(FormulaElement{ FormulaElementType::number, Fraction(number) });
                n++;
            }
        }
        else if (input[n].type == ItemType::decimal_number)
        {   // decimal_number
            //long long wholeNumber = 0;
            //long long fraction = 0;
            //if (!DecomposeStringDecimal(input[n].value, wholeNumber, fraction))
            //{
            //    return false;
            //}
            //output.push_back(FormulaElement{ FormulaElementType::number, Fraction(wholeNumber, fraction) });
            //n++;
            try
            {
                Fraction decNum(input[n].value);
                output.push_back(FormulaElement{ FormulaElementType::number, decNum });
            }
            catch (...)
            {
                return false;
            }
            n++;
        }
        else if (input[n].type == ItemType::math_operator)
        {
            if (input[n].value == "+")
            {
                output.push_back(FormulaElement{ FormulaElementType::add, 2 });
                n++;
            }
            else if (input[n].value == "-")
            {
                output.push_back(FormulaElement{ FormulaElementType::subtract, 2 });
                n++;
            }
            else if (input[n].value == "*")
            {
                output.push_back(FormulaElement{ FormulaElementType::multiply, 4 });
                n++;
            }
            else if (input[n].value == "/")
            {
                output.push_back(FormulaElement{ FormulaElementType::divide, 4 });
                n++;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    return true;
}

//
// Based on https://en.wikipedia.org/wiki/Shunting-yard_algorithm
//
bool FormulaHelper::ConstructFormulaStack(const vector<FormulaElement>& formula, vector<FormulaElement>& formulaStack)
{
    enum class State
    {
        start,
        expect_operator_or_close_bracket,   // after a number or close bracket
        expect_number_or_open_bracket,      // after open bracket or operator
    };

    vector<FormulaElement> operationStack;
    State status = State::start;
    size_t n = 0;
    while (n < formula.size())
    {
        if (formula[n].type == Formula::FormulaElementType::number)
        {
            formulaStack.push_back(FormulaElement{ formula[n].type, formula[n].number });
        }
        else
        {
            operationStack.push_back(FormulaElement{ formula[n].type, formula[n].precedence });
        }

        if ((operationStack.size() > 1) && IsOperator(operationStack.back()) && 
            (operationStack[operationStack.size() - 2].type != FormulaElementType::open_bracket) &&
            (operationStack.back().precedence > operationStack[operationStack.size() - 2].precedence))
        {
            if ((n < formula.size() - 1) && (formula[n + 1].type == FormulaElementType::number))
            {
                formulaStack.push_back(FormulaElement{ formula[n + 1].type, formula[n + 1].number });
                formulaStack.push_back(operationStack.back());
                operationStack.pop_back();
                n += 2;
            }
            else if ((n < formula.size() - 1) && (formula[n + 1].type == FormulaElementType::open_bracket))
            {
                n++;
            }
            else
            {
                return false;
            }
        }
        else if ((operationStack.size() > 1) && IsOperator(operationStack.back()) &&
                 (operationStack[operationStack.size() - 2].type != FormulaElementType::open_bracket) &&
                 (operationStack.back().precedence <= operationStack[operationStack.size() - 2].precedence))
        {
            formulaStack.push_back(operationStack[operationStack.size() - 2]);
            operationStack[operationStack.size() - 2] = operationStack.back();
            operationStack.pop_back();
            n++;
        }
        else if ((!operationStack.empty()) && (operationStack.back().type == FormulaElementType::close_bracket))
        {
            size_t size = operationStack.size();

            if ((size >= 3) && IsOperator(operationStack[size - 2])) // && (operationStack[size - 3].type == FormulaElementType::open_bracket))
            {
                vector<FormulaElement>::reverse_iterator it = find_if(operationStack.rbegin(), operationStack.rend(), [](FormulaElement e) {return (e.type == FormulaElementType::open_bracket); });
                if (it == operationStack.rend())
                {
                    return false;
                }
                else
                {
                    size_t numToPop = it - operationStack.rbegin();
                    for (int i = 0; i < numToPop - 1; i++)
                    {
                        formulaStack.push_back(operationStack[size - 2 - i]);
                    }
                    for (int i = 0; i < numToPop + 1; i++)
                    {
                        operationStack.pop_back();
                    }
                }
                //formulaStack.push_back(operationStack[size - 2]);
                //operationStack.pop_back();
                //operationStack.pop_back();
                //operationStack.pop_back();
                n++;
            }
            else if ((size >= 2) && (operationStack[size - 2].type == FormulaElementType::open_bracket))
            {
                operationStack.pop_back();
                operationStack.pop_back();
                n++;
            }
            else
            {
                return false;
            }
        }
        else
        {
            n++;
        }
    }
    while (!operationStack.empty())
    {
        formulaStack.push_back(operationStack.back());
        operationStack.pop_back();
    }
    return true;
}

bool FormulaHelper::IsOpenBracket(char c)
{
    return ((c == '(') || (c == '[') || (c == '{')) ? true : false;
}

bool FormulaHelper::IsCloseBracket(char c)
{
    return ((c == ')') || (c == ']') || (c == '}')) ? true : false;
}

bool FormulaHelper::IsSign(char c)
{
    return ((c == '+') || (c == '-')) ? true : false;
}

bool FormulaHelper::IsDecimalPoint(char c)
{
    return (c == '.') ? true : false;
}

FormulaHelper::OperatorStatus FormulaHelper::IsValidOperator(char c, string current)
{
    return ((c == '+') || (c == '-') || (c == '*') || (c == '/')) ? OperatorStatus::yes : OperatorStatus::no;
}

bool FormulaHelper::DecomposeStringDecimal(string decimal, long long& wholeNumber, long long& fraction)
{
    size_t pos = decimal.find('.');
    if ((pos == string::npos) || (pos == decimal.size() - 1))
    {
        return false;
    }
    string w = decimal.substr(0, pos);
    string f = decimal.substr(pos + 1, decimal.size() - pos - 1);
    wholeNumber = stoll(w);
    fraction = stoll(f);
    return true;
}

bool FormulaHelper::IsOperator(const FormulaElement& elt)
{
    return ((elt.type == FormulaElementType::add) || 
            (elt.type == FormulaElementType::subtract) ||
            (elt.type == FormulaElementType::multiply) || 
            (elt.type == FormulaElementType::divide));

}