#include <iostream>
#include <stdio.h>
using namespace std;
#include "fcalcCmdLine.h"
#include "FractionCalculator.h"
#include <limits>

int main(int argc, char* argv[])
{
    try
    {
        fcalcCmdLine cmdLine(argc, argv);
        //cout << "Output in fraction? " << cmdLine.OutputInFraction() << endl;
        if (cmdLine.InputFromStdin())
        {
            cout << "fcalc -h to get help. Type formula below to start calculating." << endl;
        }
        string formula;
        while ((formula = cmdLine.GetNextTextFormula()) != "")
        {
            FractionCalculator fcalc;
            Fraction result(0);
            if (!fcalc.Calculate(formula, result))
            {
                cerr << "Error in formula: " << formula;
                if (cmdLine.InputFromStdin())
                {
                    cerr << endl;
                }
                else
                {
                    cerr << " | Usage: " << cmdLine.usage << endl;
                }
            }
            else
            {
                if (cmdLine.Verbose())
                {
                    cout << formula << " = ";
                }
                if (cmdLine.OutputInFraction())
                {
                    if (result.Sign() == -1)
                    {
                        cout << "-";
                    }
                    if (result.WholeNum() > 0)
                    {
                        cout << result.WholeNum();
                    }
                    if (result.Numerator() != 0)
                    {
                        if (result.WholeNum() > 0)
                        {
                            cout << " ";
                        }
                        cout << result.Numerator() << "/" << result.Denominator();
                    }
                    cout << endl;
                }
                else
                {
                    cout.precision(std::numeric_limits<double>::digits10 + 1);
                    cout << result.Decimal() << endl;
                }
            }
        }
    }
    catch (string msg)
    {
        cerr << msg << endl;
        return 1;
    }
}