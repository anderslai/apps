#pragma once
#include <string>
#include "Fraction.h"

using namespace std;

class FractionCalculator
{
public:
    bool Calculate(string formula, Fraction& result);
};