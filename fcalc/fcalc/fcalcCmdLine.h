#pragma once
#include <string>
using namespace std;
#include <fstream>

// fcalc                        // result in fraction, get formula from stdin, result output to stdout
//
// fcalc [(2 + 1) * 6] - 8		// result in fraction, formula from argument, result output to stdout
//
// fcalc -d [(2 + 1) * 6] - 8   // result in decimal, formula from argument, result output to stdout
// 
// fcalc -d -f a.txt		    // result in decimal; formula in file, result output to stdout
//
// fcalc -f a.txt			    // result in fraction, formula in file, result output to stdout
//
// fcalc -v -f a.txt            // same as above except that -v includes formula in result output

class fcalcCmdLine
{
public:
    fcalcCmdLine() = delete;
    fcalcCmdLine(int argc, char* argv[]);   // throw exception message string
    ~fcalcCmdLine();
    bool OutputInFraction() const;
    bool Verbose() const;
    bool InputFromStdin() const;
    string GetNextTextFormula();            // null if end of input
    const string usage;

private:
    bool outputInFraction;
    bool verbose;

    enum class InputSource
    {
        standard_input,
        file,
        command_line
    };
    InputSource inputSource;
    ifstream    ifs;

    string      formula;
};