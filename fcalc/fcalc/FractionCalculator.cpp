#include "FractionCalculator.h"
#include "FormulaHelper.h"
#include <algorithm>

bool FractionCalculator::Calculate(string formula, Fraction& result)
{
    Formula::FormulaHelper helper;
    vector<Formula::Item> items;
    if (!helper.Text2Items(formula, items))
    {
        return false;
    }
    if (!helper.CheckBracketSanity(items))
    {
        return false;
    }
    vector<Formula::FormulaElement> formulaElts;
    if (!helper.ItemsToFormula(items, formulaElts))
    {
        return false;
    }
    vector<Formula::FormulaElement> formulaStack;
    if (!helper.ConstructFormulaStack(formulaElts, formulaStack))
    {
        return false;
    }
    size_t originalFormulaStackSize = formulaStack.size();

    auto findOp = [](const Formula::FormulaElement& e)
    {
        return ((e.type == Formula::FormulaElementType::add) ||
                (e.type == Formula::FormulaElementType::subtract) ||
                (e.type == Formula::FormulaElementType::multiply) ||
                (e.type == Formula::FormulaElementType::divide));
    };
    
    /*int n = 0;
    while (n != formulaElts.size())
    {
        for (n = 0; n < formulaElts.size(); n++)
        {
            if (findOp(formulaElts[n]))
            {
                break;
            }
        }
    }*/

    vector<Formula::FormulaElement>::iterator it = find_if(formulaStack.begin(), formulaStack.end(), findOp);
    while (it != formulaStack.end())
    {
        if ((it - formulaStack.begin()) >= 2)
        {
            if ( ((*(it - 2)).type == Formula::FormulaElementType::number) &&
                 ((*(it - 1)).type == Formula::FormulaElementType::number) )
            {
                size_t pos = it - formulaStack.begin();
                switch ((*it).type)
                {
                case Formula::FormulaElementType::add :
                {
                    formulaStack[pos - 2].number = formulaStack[pos - 2].number + formulaStack[pos - 1].number;
                    break;
                }
                case Formula::FormulaElementType::subtract:
                {
                    formulaStack[pos - 2].number = formulaStack[pos - 2].number - formulaStack[pos - 1].number;
                    break;
                }
                case Formula::FormulaElementType::multiply:
                {
                    formulaStack[pos - 2].number = formulaStack[pos - 2].number * formulaStack[pos - 1].number;
                    break;
                }
                case Formula::FormulaElementType::divide:
                {
                    formulaStack[pos - 2].number = formulaStack[pos - 2].number / formulaStack[pos - 1].number;
                    break;
                }
                }
                formulaStack.erase(it - 1, it + 1);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        it = find_if(formulaStack.begin(), formulaStack.end(), findOp);
    }
    if (formulaStack.size() == 1)
    {
        result = formulaStack[0].number;
        if (originalFormulaStackSize == 1)
        {
            result.Simplify();
        }
        return true;
    }
    return false;
}