#include "Fraction.h"
#include "PrimeNum.h"
#include <stdexcept>
#include <exception>
#include <math.h>

Fraction::Fraction(long long wholeNum) :
    wholeNum((wholeNum >= 0) ? wholeNum : wholeNum * (-1)),
    numerator(0),
    denominator(1),
    positive((wholeNum >= 0))
{
}

Fraction::Fraction(long long wholeNum, long long fraction) :
    wholeNum((wholeNum >= 0) ? wholeNum : wholeNum * (-1)),
    numerator((fraction >= 0) ? fraction : fraction * (-1)),
    denominator(GetDenominatorFor(numerator)),
    positive((wholeNum >= 0) && (fraction >= 0))
{
}

Fraction::Fraction(long long wholeNum, long long numerator, long long denominator) :
    wholeNum((wholeNum >= 0) ? wholeNum : wholeNum * (-1)),
    numerator((numerator >= 0) ? numerator : numerator * (-1)),
    denominator((denominator >= 0) ? denominator : denominator * (-1)),
    positive((wholeNum >= 0) && (numerator >= 0) && (denominator >= 0))
{
    if (denominator == 0)
    {
        throw std::invalid_argument("zero denominator");
    }
}

Fraction::Fraction(string decimal) :
    positive(true)
{
    string decimal_num = decimal;
    if (decimal.size() > 1)
    {
        if (decimal[0] == '-')
        {
            positive = false;
            decimal_num = decimal.substr(1, decimal.size() - 1);
        }
        else if (decimal[0] == '+')
        {
            //positive = true;
            decimal_num = decimal.substr(1, decimal.size() - 1);
        }
    }
    size_t pos = decimal_num.find('.');
    if ((pos == string::npos) || (pos == decimal_num.size() - 1))
    {
        throw std::invalid_argument("cannot find decimal point");
    }
    string w = decimal_num.substr(0, pos);
    for (auto c : w)
    {
        if (!isdigit(c))
        {
            throw std::invalid_argument("non-digit found");
        }
    }
    string f = decimal_num.substr(pos + 1, decimal_num.size() - pos - 1);
    int numDigits = 0;
    for (auto c : f)
    {
        if (!isdigit(c))
        {
            throw std::invalid_argument("non-digit found");
        }
        else
        {
            numDigits++;
        }
    }
    wholeNum = stoll(w);
    numerator = stoll(f);
    denominator = (long long)1;
    for (int n = 0; n < numDigits; n++)
    {
        denominator = denominator * 10;
    }
}

Fraction Fraction::operator+(const Fraction& rhs)
{
    Fraction val1(SignedWholeNum(), SignedNumerator(), SignedDenominator());
    val1.Simplify();   
    Fraction val2(rhs.SignedWholeNum(), rhs.SignedNumerator(), rhs.SignedDenominator());
    val2.Simplify();
    
    if ((val1.Denominator() == 1) || (val2.Denominator() == 1))
    {
        if (val1.Denominator() == 1)
        {
            long long n1 = (val1.WholeNum() * val2.Denominator() + val1.Numerator()) * val1.Sign();
            long long n2 = (val2.WholeNum() * val2.Denominator() + val2.Numerator()) * val2.Sign();
            Fraction result(0, n1 + n2, val2.Denominator());
            result.Simplify();
            return result;
        }
        else
        {
            long long n1 = (val1.WholeNum() * val1.Denominator() + val1.Numerator()) * val1.Sign();
            long long n2 = (val2.WholeNum() * val1.Denominator() + val2.Numerator()) * val2.Sign();
            Fraction result(0, n1 + n2, val1.Denominator());
            result.Simplify();
            return result;
        }
    }
    else
    {
        long long lcm = GetLCM(val1, val2);
        long long val1Multiple = lcm / val1.Denominator();
        long long val2Multiple = lcm / val2.Denominator();

        long long n1 = ( val1.WholeNum() * val1.Denominator() * val1Multiple + val1.Numerator() * val1Multiple ) * val1.Sign();
        long long n2 = ( val2.WholeNum() * val2.Denominator() * val2Multiple + val2.Numerator() * val2Multiple ) * val2.Sign();
        Fraction result(0, n1 + n2, lcm);
        result.Simplify();
        return result;
    }
}

Fraction Fraction::operator-(const Fraction& rhs)
{
    Fraction val1(SignedWholeNum(), SignedNumerator(), SignedDenominator());
    val1.Simplify();
    Fraction val2(rhs.SignedWholeNum(), rhs.SignedNumerator(), rhs.SignedDenominator());
    val2.Simplify();

    if ((val1.Denominator() == 1) || (val2.Denominator() == 1))
    {
        long long n1 = ( val1.WholeNum() * val1.Denominator() + val1.Numerator() ) * val1.Sign();
        long long n2 = ( val2.WholeNum() * val2.Denominator() + val2.Numerator() ) * val2.Sign();
        if (val1.Denominator() == 1)
        {
            Fraction result(0, n1 * val2.Denominator() - n2, val2.Denominator());
            result.Simplify();
            return result;
        }
        else
        {
            Fraction result(0, n1 - n2 * val1.Denominator(), val1.Denominator());
            result.Simplify();
            return result;
        }
    }
    else
    {
        long long lcm = GetLCM(val1, val2);
        long long val1Multiple = lcm / val1.Denominator();
        long long val2Multiple = lcm / val2.Denominator();

        long long n1 = ( val1.WholeNum() * val1.Denominator() * val1Multiple + val1.Numerator() * val1Multiple ) * val1.Sign();
        long long n2 = ( val2.WholeNum() * val2.Denominator() * val2Multiple + val2.Numerator() * val2Multiple ) * val2.Sign();
        Fraction result(0, n1 - n2, lcm);
        result.Simplify();
        return result;
    }
}
Fraction Fraction::operator*(const Fraction& rhs)
{
    Fraction val1(SignedWholeNum(), SignedNumerator(), SignedDenominator());
    val1.Simplify();
    Fraction val2(rhs.SignedWholeNum(), rhs.SignedNumerator(), rhs.SignedDenominator());
    val2.Simplify();

    long long n1 = ( val1.WholeNum() * val1.Denominator() + val1.Numerator() ) * val1.Sign();
    long long n2 = ( val2.WholeNum() * val2.Denominator() + val2.Numerator() ) * val2.Sign();

    Fraction result(0, n1 * n2, val1.Denominator() * val2.Denominator());
    result.Simplify();
    return result;    
}

Fraction Fraction::operator/(const Fraction& rhs)
{
    Fraction val1(SignedWholeNum(), SignedNumerator(), SignedDenominator());
    val1.Simplify();
    Fraction val2(rhs.SignedWholeNum(), rhs.SignedNumerator(), rhs.SignedDenominator());
    val2.Simplify();

    long long n1 = ( val1.WholeNum() * val1.Denominator() + val1.Numerator() ) * val1.Sign();
    long long n2 = ( val2.WholeNum() * val2.Denominator() + val2.Numerator() ) * val2.Sign();
    
    long long d = val1.Denominator() * n2;
    if (d == 0)
    {
        throw overflow_error("division by zero");
    }
    long long n = n1 * val2.Denominator();
    if ((n < 0) && (d < 0))
    {
        n = n * (-1);
        d = d * (-1);
    }
    Fraction result(0, n, d);
    result.Simplify();
    return result;
}

long long Fraction::WholeNum() const
{
    return wholeNum;
}

long long Fraction::Numerator() const
{
    return numerator;
}

long long Fraction::Denominator() const
{
    return denominator;
}

int Fraction::Sign() const
{
    return ((positive == true) ? 1 : -1);
}

double Fraction::Decimal() const
{
    double d = (double)numerator / (double)denominator;
    d += wholeNum;
    d = d * Sign();
    return d;
}

void Fraction::Simplify(void)
{
    if (denominator == 0)
    {
        return;
    }
    if (denominator == 1)
    {
        wholeNum += numerator;
        numerator = 0;
        return;
    }

    long long multiple = numerator / denominator;
    wholeNum += multiple;
    numerator -= multiple * denominator;
    
    if (numerator == 0)
    {
        denominator = 1;
        return;
    }

    PrimeNum prime;
    vector<long long> numeratorFactors = prime.FindPrimeFactors(numerator);
    vector<long long> denominatorFactors = prime.FindPrimeFactors(denominator);
    map<long long, int> nFactors = SummarizeFactors(numeratorFactors);
    map<long long, int> dFactors = SummarizeFactors(denominatorFactors);
    
    for (auto n : nFactors)
    {
        if (dFactors.find(n.first) != dFactors.end())       
        {
            if (n.second >= dFactors[n.first])
            {
                //n.second -= dFactors[n.first];
                nFactors[n.first] -= dFactors[n.first];
                dFactors[n.first] = 0;
            }
            else
            {
                dFactors[n.first] -= n.second;
                //n.second = 0;
                nFactors[n.first] = 0;
            }
        }
    }
    numerator = 1;
    for (auto n : nFactors)
    {
        for (int c = 0; c < n.second; c++)
        {
            numerator = numerator * n.first;
        }
    }
    denominator = 1;
    for (auto n : dFactors)
    {
        for (int c = 0; c < n.second; c++)
        {
            denominator = denominator * n.first;
        }
    }
}

long long Fraction::GetLCM(const Fraction& f1, const Fraction& f2)
{
    PrimeNum prime;

    Fraction v1(f1.WholeNum(), f1.Numerator(), f1.Denominator());
    v1.Simplify();
    vector<long long> factors1 = prime.FindPrimeFactors(v1.Denominator());
    map<long long, int> sumFactors1 = SummarizeFactors(factors1);

    Fraction v2(f2.WholeNum(), f2.Numerator(), f2.Denominator());
    v2.Simplify();
    vector<long long> factors2 = prime.FindPrimeFactors(v2.Denominator());
    map<long long, int> sumFactors2 = SummarizeFactors(factors2);

    map<long long, int> lcmSumFactors;

    for (auto x1 : sumFactors1)
    {
        if (sumFactors2.find(x1.first) != sumFactors2.end())
        {
            if (x1.second >= sumFactors2[x1.first])
            {
                lcmSumFactors[x1.first] = x1.second;
            }
            else
            {
                lcmSumFactors[x1.first] = sumFactors2[x1.first];
            }
        }
        else
        {
            lcmSumFactors[x1.first] = x1.second;
        }
    }
    for (auto x2 : sumFactors2)
    {
        if (sumFactors1.find(x2.first) == sumFactors1.end())
        {
            lcmSumFactors[x2.first] = x2.second;
        }
    }

    long long lcm = 1;
    for (auto f : lcmSumFactors)
    {
        for (int t = 1; t <= f.second; t++)
        {
            lcm = lcm * f.first;
        }
    }
    return lcm;
}

long long Fraction::GetDenominatorFor(long long number)
{
    //int t = 1;
    long long base = 10;
    while (number >= base)
    {
        base = base * (long long)10;
    }
    return base;
}

map<long long, int> Fraction::SummarizeFactors(vector<long long>& factors)
{
    map<long long, int> result;
    long long current = 0;
    for (auto f : factors)
    {
        if (current != f)
        {
            result[f] = 1;
            current = f;
        }
        else
        {
            result[f]++;
        }
    }
    return result;
}