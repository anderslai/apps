#include "stdafx.h"
#include "CppUnitTest.h"
#include "../caesar/CaesarCmdLine.h"
#include <string.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
    TEST_CLASS(CaesarCmdLineUT)
    {
    public:

        TEST_METHOD(CaesarCmdLine_NoArg)
        {
            char* argv[1];
            argv[0] = new char[10];
            strcpy_s(argv[0], 9, "caesar");
            CaesarCmdLine cmdLine(1, argv);
            Assert::AreEqual(cmdLine.Validated(), false);
            Assert::AreEqual(cmdLine.Option(), 0);
            Assert::AreEqual(cmdLine.InFile().compare(""), 0);
            Assert::AreEqual(cmdLine.OutFile().compare(""), 0);
            delete [] argv[0];
        }

        TEST_METHOD(CaesarCmdLine_InvalidArg1)
        {
            char* argv[2];
            argv[0] = new char[10];
            argv[1] = new char[10];
            strcpy_s(argv[0], 9, "caesar");
            strcpy_s(argv[1], 10, "blah");
            CaesarCmdLine cmdLine(2, argv);
            Assert::AreEqual(cmdLine.Validated(), false);
            //Assert::AreEqual(cmdLine.Option(), 0);
            //Assert::AreEqual(cmdLine.InFile().compare(""), 0);
            //Assert::AreEqual(cmdLine.OutFile().compare(""), 0);
            delete[] argv[0];
            delete[] argv[1];
        }

        TEST_METHOD(CaesarCmdLine_Valid_EncryptWithoutFile) 
        {
            char* argv[2];
            argv[0] = new char[10];
            argv[1] = new char[10];
            strcpy_s(argv[0], 9, "caesar");
            strcpy_s(argv[1], 9, "-encrypt");
            CaesarCmdLine cmdLine(2, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), 1);
            Assert::AreEqual(cmdLine.InFile().compare(""), 0);
            Assert::AreEqual(cmdLine.OutFile().compare(""), 0);
            delete[] argv[0];
            delete[] argv[1];
        }

        TEST_METHOD(CaesarCmdLine_Valid_DecryptWithoutFile)
        {
            char* argv[2];
            argv[0] = new char[10];
            argv[1] = new char[10];
            strcpy_s(argv[0], 9, "caesar");
            strcpy_s(argv[1], 9, "-decrypt");
            CaesarCmdLine cmdLine(2, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), -1);
            Assert::AreEqual(cmdLine.InFile().compare(""), 0);
            Assert::AreEqual(cmdLine.OutFile().compare(""), 0);
            delete[] argv[0];
            delete[] argv[1];
        }

        TEST_METHOD(CaesarCmdLine_Valid_EncryptWith1File1)
        {
            char* argv[24];
            argv[0] = new char[50];
            argv[1] = new char[50];
            argv[2] = new char[50];
            argv[3] = new char[50];
            strcpy_s(argv[0], 49, "caesar");
            strcpy_s(argv[1], 49, "-encrypt");
            strcpy_s(argv[2], 49, "-infile");
            strcpy_s(argv[3], 49, "plaintextfile");           
            CaesarCmdLine cmdLine(4, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), 1);
            Assert::AreEqual(cmdLine.InFile().compare("plaintextfile"), 0);
            Assert::AreEqual(cmdLine.OutFile().compare(""), 0);
            delete[] argv[0];
            delete[] argv[1];
            delete[] argv[2];
            delete[] argv[3];
        }

        TEST_METHOD(CaesarCmdLine_Valid_EncryptWith1File2)
        {
            char* argv[24];
            argv[0] = new char[50];
            argv[1] = new char[50];
            argv[2] = new char[50];
            argv[3] = new char[50];
            strcpy_s(argv[0], 49, "caesar");
            strcpy_s(argv[1], 49, "-outfile");
            strcpy_s(argv[2], 49, "cypherfile");
            strcpy_s(argv[3], 49, "-encrypt");
            CaesarCmdLine cmdLine(4, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), 1);
            Assert::AreEqual(cmdLine.InFile().compare(""), 0);
            Assert::AreEqual(cmdLine.OutFile().compare("cypherfile"), 0);
            delete[] argv[0];
            delete[] argv[1];
            delete[] argv[2];
            delete[] argv[3];
        }

        TEST_METHOD(CaesarCmdLine_Valid_DecryptWith1File3)
        {
            char* argv[24];
            argv[0] = new char[50];
            argv[1] = new char[50];
            argv[2] = new char[50];
            argv[3] = new char[50];
            strcpy_s(argv[0], 49, "caesar");
            strcpy_s(argv[1], 49, "-decrypt");
            strcpy_s(argv[2], 49, "-outfile");
            strcpy_s(argv[3], 49, "cypherfile");

            CaesarCmdLine cmdLine(4, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), -1);
            Assert::AreEqual(cmdLine.InFile().compare(""), 0);
            Assert::AreEqual(cmdLine.OutFile().compare("cypherfile"), 0);
            delete[] argv[0];
            delete[] argv[1];
            delete[] argv[2];
            delete[] argv[3];
        }

        TEST_METHOD(CaesarCmdLine_Valid_DecryptWith1File4)
        {
            char* argv[24];
            argv[0] = new char[50];
            argv[1] = new char[50];
            argv[2] = new char[50];
            argv[3] = new char[50];
            strcpy_s(argv[0], 49, "caesar");
            strcpy_s(argv[1], 49, "-decrypt");
            strcpy_s(argv[2], 49, "-file");
            strcpy_s(argv[3], 49, "cypherfile");

            CaesarCmdLine cmdLine(4, argv);
            Assert::AreEqual(cmdLine.Validated(), false);
            delete[] argv[0];
            delete[] argv[1];
            delete[] argv[2];
            delete[] argv[3];
        }

        TEST_METHOD(CaesarCmdLine_Valid_EncryptWith2File1)
        {
            char* argv[24];
            argv[0] = new char[50];
            argv[1] = new char[50];
            argv[2] = new char[50];
            argv[3] = new char[50];
            argv[4] = new char[50];
            argv[5] = new char[50];
            strcpy_s(argv[0], 49, "caesar");
            strcpy_s(argv[1], 49, "-encrypt");
            strcpy_s(argv[2], 49, "-infile");
            strcpy_s(argv[3], 49, "textfile");
            strcpy_s(argv[4], 49, "-outfile");
            strcpy_s(argv[5], 49, "cypherfile");

            CaesarCmdLine cmdLine(6, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), 1);
            Assert::AreEqual(cmdLine.InFile().compare("textfile"), 0);
            Assert::AreEqual(cmdLine.OutFile().compare("cypherfile"), 0);
            delete[] argv[0];
            delete[] argv[1];
            delete[] argv[2];
            delete[] argv[3];
            delete[] argv[4];
            delete[] argv[5];
        }

        TEST_METHOD(CaesarCmdLine_Valid_DecryptWith2File1)
        {
            char* argv[24];
            argv[0] = new char[50];
            argv[1] = new char[50];
            argv[2] = new char[50];
            argv[3] = new char[50];
            argv[4] = new char[50];
            argv[5] = new char[50];
            strcpy_s(argv[0], 49, "caesar");
            strcpy_s(argv[1], 49, "-outfile");
            strcpy_s(argv[2], 49, "textfile");
            strcpy_s(argv[3], 49, "-decrypt");
            strcpy_s(argv[4], 49, "-infile");
            strcpy_s(argv[5], 49, "cypherfile");
            CaesarCmdLine cmdLine(6, argv);
            Assert::AreEqual(cmdLine.Validated(), true);
            Assert::AreEqual(cmdLine.Option(), -1);
            Assert::AreEqual(cmdLine.InFile().compare("cypherfile"), 0);
            Assert::AreEqual(cmdLine.OutFile().compare("textfile"), 0);
            delete[] argv[0];
            delete[] argv[1];
            delete[] argv[2];
            delete[] argv[3];
            delete[] argv[4];
            delete[] argv[5];
        }
    };
}