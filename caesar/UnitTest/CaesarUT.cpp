#include "stdafx.h"
#include "CppUnitTest.h"
#include "../caesar/caesar.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{		
	TEST_CLASS(CaesarUT)
	{
	public:
		
		TEST_METHOD(SimpleTest)    // text longer than key
		{
            string cypher;
            bool b = Caesar::Encrypt("AAA|}~", "!#$", cypher);
            Assert::AreEqual(b, true);
            Assert::AreEqual(cypher.compare("BDE}!#"), 0);

            string text;
            b = Caesar::Decrypt(cypher, "!#$", text);
            Assert::AreEqual(b, true);
            Assert::AreEqual(text.compare("AAA|}~"), 0);
		}

        TEST_METHOD(TypicalUseTest)    // text longer than key
        {
            string cypher;
            bool b = Caesar::Encrypt("T0ttenh@m H0t$pur$", "WhiteH@rtL@ne", cypher);
            Assert::AreEqual(b, true);
            Assert::AreEqual(cypher.compare(",x^iK7)3bLh~Z[Y_gi"), 0);

            string text;
            b = Caesar::Decrypt(cypher, "WhiteH@rtL@ne", text);
            Assert::AreEqual(b, true);
            Assert::AreEqual(text.compare("T0ttenh@m H0t$pur$"), 0);
        }

        TEST_METHOD(EmptyKey)
        {
            string cypher;
            bool b = Caesar::Encrypt("AAA|}~", "", cypher);
            Assert::AreEqual(b, false);

            string text;
            b = Caesar::Decrypt("BDE}!#", "", cypher);
            Assert::AreEqual(b, false);
        }

        TEST_METHOD(EmptyText)
        {
            string cypher;
            bool b = Caesar::Encrypt("", "ABC", cypher);
            Assert::AreEqual(b, false);
        }

        TEST_METHOD(EmptyCypher)
        {
            string text;
            bool b = Caesar::Decrypt("BDE}!#", "", text);
            Assert::AreEqual(b, false);
        }
    };
}