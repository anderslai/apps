#include "CaesarCmdLine.h"
#include <string.h>
#include <iostream>

CaesarCmdLine::CaesarCmdLine(int argc, char* argv[])
{
    string msg;
    if (!Validate(argc, argv, msg))
    {
        cout << msg;
    }
}

int CaesarCmdLine::Option() const
{
    return option;
}

string CaesarCmdLine::InFile() const
{
    return infile;
}

string CaesarCmdLine::OutFile() const
{
    return outfile;
}

bool CaesarCmdLine::Validated() const
{
    return validated;
}

bool CaesarCmdLine::Validate(int argc, char* argv[], string& msg)
{
    if (argc == 2)
    {
        if (strcmp(argv[1], "-encrypt") == 0)
        {
            option = 1;
            validated = true;
            return true;
        }
        if (strcmp(argv[1], "-decrypt") == 0)
        {
            option = -1;
            validated = true;
            return true;
        }
    }

    if ((argc == 6) || (argc == 4))
    {
        int cnt = 1;
        int state = 0;      // 0 -->
                            // 1 --> expect infile
                            // 2 --> expect outfile
        bool validSyntax = true;
        while (cnt < argc)
        {
            char* arg = argv[cnt];
            if (state == 1)
            {
                infile = arg;
                state = 0;
            }
            else if (state == 2)
            {
                outfile = arg;
                state = 0;
            }
            else             // state == 0
            {
                if (strcmp(arg, "-encrypt") == 0)
                {
                    option = 1;
                }
                else if (strcmp(arg, "-decrypt") == 0)
                {
                    option = -1;
                }
                else if (strcmp(arg, "-infile") == 0)
                {
                    state = 1;
                }
                else if (strcmp(arg, "-outfile") == 0)
                {
                    state = 2;
                }
                else
                {
                    validSyntax = false;
                    break;
                }
            }
            cnt++;
        }
        if (validSyntax)
        {
            validated = true;
            return true;
        }
    }

    msg =  "caesar -encrypt {-infile [text_file]} {-outfile [cypher_file]}\n";
    msg += "caesar -decript {-infile [cypher_file]} {-outfile [text_file]}\n";
    option = 0;
    validated = false;
    return false;
}