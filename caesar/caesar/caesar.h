#pragma once
#include <string>
using namespace std;

class Caesar
{
public:
    static bool Encrypt(string text, string key, string& cypher);
    static bool Decrypt(string cypher, string key, string& text);

private:
    static const char asciiStart = ' ';
    static const char asciiEnd = '~';

    static char GetNextKeyChar(string cypher, int& index);  // index = index of last
};