#include "caesar.h"


bool Caesar::Encrypt(string text, string key, string& cypher)
{
    int textLen = text.size();
    int keyLen = key.size();
    if ((textLen == 0) || (keyLen == 0))
    {
        return false;   // cypher left untouched
    }

    int keyCharIdx = 0;
    cypher = "";
    for (auto t : text)
    {
        if ((t >= asciiStart) && (t <= asciiEnd))
        {
            int shift = key[keyCharIdx] - asciiStart;
            char c = ((t + shift) <= asciiEnd) ? (t + shift) : (t + shift - asciiEnd + asciiStart - 1);
            cypher += c;
            keyCharIdx = (keyCharIdx < (keyLen - 1)) ? (keyCharIdx + 1) : 0;
        }
        else
        {
            return false;
        }
    }

    return true;
}


bool Caesar::Decrypt(string cypher, string key, string& text)
{
    int cypherLen = cypher.size();
    int keyLen = key.size();
    if ((cypherLen == 0) || (keyLen == 0))
    {
        return false;   // cypher left untouched
    }

    int keyCharIdx = 0;
    text = "";
    for (auto c : cypher)
    {
        if ((c >= asciiStart) && (c <= asciiEnd))
        {
            int t = ((c - key[keyCharIdx]) >= 0) ? (c - key[keyCharIdx] + asciiStart) : (c - key[keyCharIdx] + asciiEnd + 1);
            text += t;
            keyCharIdx = (keyCharIdx < (keyLen - 1)) ? (keyCharIdx + 1) : 0;
        }
        else
        {
            return false;
        }
    }

    return true;
}


char Caesar::GetNextKeyChar(string cypher, int& index)
{
    if (index >= (cypher.size() - 1))
    {
        index = 0;
        return cypher[0];
    }
    else
    {
        return cypher[index++];
    }
}