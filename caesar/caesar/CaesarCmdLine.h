#pragma once
#include <string>
using namespace std;

class CaesarCmdLine
{
    // caesar
    //      display usage
    //
    // caesar -encrypt
    // text:
    // key:
    // cypher: 
    //
    // caesar -decrypt
    // cypher:
    // key:
    // text:
    //
    // caesar -encrypt -text [infile] -cypher [outfile]
    // key:
    //
    // caesar -decrypt -cypher [infile] -text [outfile]
    // key:
    //

public:
    CaesarCmdLine(int argc, char* argv[]);
    int Option() const;
    string InFile() const;
    string OutFile() const;
    bool Validated() const;

private:
    bool Validate(int argc, char* argv[], string& msg);

    int option;     // -1   decrypt
                    //  0   undefined
                    //  1   encrypt
    string infile;
    string outfile;
    bool validated;
};
