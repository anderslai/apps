#include "CaesarCmdLine.h"
#include "caesar.h"
#include <iostream>
#include <fstream>
#include <windows.h>

class Main
{
public:
    Main() {}
    ~Main()
    {
        if (ifs.is_open())
        {
            ifs.close();
        }
        if (ofs.is_open())
        {
            ofs.close();
        }
    }
    int Run(int argc, char* argv[]);

private:
    ifstream    ifs;
    ofstream    ofs;

    // helper functions
    string GetKey(void);
    string GetText(string prompt);
    bool EncryptAndOutput(CaesarCmdLine& cmdLine, string text, string key, ofstream& ofs);
    bool DecryptAndOutput(CaesarCmdLine& cmdLine, string cypher, string key, ofstream& ofs);
};

string Main::GetKey(void)
{
    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode = 0;
    GetConsoleMode(hStdin, &mode);
    SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));

    string key;
    cout << "key: ";
    cin >> key;
    cout << endl;
    return key;
}

string Main::GetText(string prompt)
{
    //string text;
    cout << prompt;
    //cin >> text;
    char textBuf[1024];
    cin.getline(textBuf, 1023);
    //text = textBuf;
    return textBuf;
}

bool Main::EncryptAndOutput(CaesarCmdLine& cmdLine, string text, string key, ofstream& ofs)
{
    string cypher;
    if (Caesar::Encrypt(text, key, cypher))
    {
        if (cmdLine.OutFile() == "")
        {
            cout << "cyphertext: " << cypher << endl;
        }
        else
        {
            ofs << cypher;
        }
    }
    else
    {
        cerr << "encrypt error" << endl;
        return false;
    }
    return true;
}

bool Main::DecryptAndOutput(CaesarCmdLine& cmdLine, string cypher, string key, ofstream& ofs)
{
    string text;
    if (Caesar::Decrypt(cypher, key, text))
    {
        if (cmdLine.OutFile() == "")
        {
            cout << "plain text: " << text << endl;
        }
        else
        {
            ofs << text;
        }

    }
    else
    {
        cerr << "decrypt error" << endl;
        return false;
    }
    return true;
}

int Main::Run(int argc, char* argv[])
{
    CaesarCmdLine cmdLine(argc, argv);

    if (!cmdLine.Validated())
    {
        return -1;
    }
    if (cmdLine.InFile() != "")
    {
        ifs.open(cmdLine.InFile(), ifstream::in);
        if (ifs.is_open())
        {
            //cout << "infile opened" << endl;
        }
        else
        {
            cerr << "Failed to open infile " << cmdLine.InFile() << endl;
            return -2;
        }
    }
    if (cmdLine.OutFile() != "")
    {
        ofs.open(cmdLine.OutFile(), ofstream::out | ofstream::trunc);
        if (ofs.is_open())
        {
            //cout << "outfile opened" << endl;
        }
        else
        {
            cerr << "Failed to open outfile " << cmdLine.OutFile() << endl;
            return -3;
        }
    }
    if (cmdLine.Option() == 1)  // encrypt
    {
        if (cmdLine.InFile() == "")
        {
            string text = GetText("plain text:");
            string key = GetKey();
            if (!EncryptAndOutput(cmdLine, text, key, ofs))
            {
                return -4;
            }
        }
        else
        {
            string key = GetKey();
            string text;
            while (getline(ifs, text))
            {
                if (!EncryptAndOutput(cmdLine, text, key, ofs))
                {
                    return -4;
                }
                ofs << endl;
            }
        }
    }
    else if (cmdLine.Option() == -1)
    {
        if (cmdLine.InFile() == "")
        {
            string cypher = GetText("cyphertext:");
            string key = GetKey();
            if (!DecryptAndOutput(cmdLine, cypher, key, ofs))
            {
                return -5;
            }
        }
        else
        {
            string key = GetKey();
            string cypher;
            while (getline(ifs, cypher))
            {
                if (!DecryptAndOutput(cmdLine, cypher, key, ofs))
                {
                    return -5;
                }
                ofs << endl;
            }
        }
    }
    return 0;
}


int main(int argc, char* argv[])
{
    Main program;
    return program.Run(argc, argv);
}